require File.expand_path('../boot', __FILE__)

require 'rails/all'

# If you have a Gemfile, require the gems listed there, including any gems
# you've limited to :test, :development, or :production.
#Bundler.require(:default, Rails.env) if defined?(Bundler)

if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
     Bundler.require *Rails.groups(:assets => %w(development test))
  # If you want your assets lazily compiled in production, use this line
  # Bundler.require(:default, :assets, Rails.env)
end



#require 'tlsmail'    
#Net::SMTP.enable_tls(OpenSSL::SSL::VERIFY_NONE)

module Causeoftheday
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    # config.autoload_paths += %W(#{config.root}/extras)
    config.autoload_paths += %W(#{config.root}/lib)
    config.autoload_paths += Dir["#{config.root}/lib/**/"]
    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # JavaScript files you want as :defaults (application.js is always included).
    Paperclip::Railtie.insert
    config.action_view.javascript_expansions[:defaults] = %w(jquery rails)

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"
    config.filter_parameters << :password << :password_confirmation

    config.generators do |g|
      g.template_engine :haml
    end

    config.logger = Logger.new(STDOUT) if $0 == 'irb'

    Dir.glob("./lib/*.{rb}").each { |file| require file }

    # Configure sensitive parameters which will be filtered from the log file.
    #    config.filter_parameters += [:password]
    #
    #Enable assets pipeline
    config.assets.enabled = true

    #Change this if you want to expire all old assets
    config.assets.version = '0.0.1'

    #exception notification -
    unless Rails.env.development?
        config.middleware.use ::ExceptionNotifier,
             :email_prefix => "[ 97Changes #{Rails.env} Errors ] ",
             :sender_address => %w{97changes1@gmail.com},
             :exception_recipients => %w{rtdp@rainingclouds.com}
    end

    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
       :address                         => "smtp.gmail.com",
       :port                              => 587,
       :user_name                   => '97changes1@gmail.com',
       :password                     => '97changes123',
       :authentication             => :plain,
       :domain                          => 'gmail.com',
       :tls                                 => true,
       :enable_starttls_auto    => true
    }

  end
end


AMOUNT = 'USD 1'
#Used Digest::SHA1.hexdigest(Time.now.to_s + 'encrypt_decrypt_for_causeoftheday')[1..20] to
#generate the ENCRYPTED_KEY and ENCRYPTED_IV for encryption of the credit cards
ENCRYPTED_KEY_FOR_CREDIT_CARD = "d938d283eb4f28f07865"
ENCRYPTED_KEY_FOR_SECURITY_CODE  = "be1467d9a2a1264e91da"


