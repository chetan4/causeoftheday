require 'config_reader'

class SiteConfig < ConfigReader
  self.config_file = File.join(Rails.root, "config", "site_config.yml")
end

#SITE_CONFIG = YAML::load(File.open("#{Rails.root}/config/site_config.yml"))
