require 'openid/store/filesystem'


Rails.application.config.middleware.use OmniAuth::Builder do
  #  provider :twitter, 'CONSUMER_KEY', 'CONSUMER_SECRET'
  provider :twitter, SiteConfig.twitter.consumer_key, SiteConfig.twitter.consumer_secret, :client_options => {:ssl => {:ca_path => "/etc/ssl/certs"}}

  #  provider :facebook, 'APP_ID', 'APP_SECRET'
  provider :facebook, SiteConfig.facebook_app.app_id, SiteConfig.facebook_app.app_secret, :scope => "email, publish_stream", :client_options => {:ssl => {:ca_path => "/etc/ssl/certs"}}

  #  provider :linked_in, 'KEY', 'SECRET'
  provider :linked_in, 'DONS9c79q9ji8baNtpbheiUQ0R5KAjbwIZmGfd4zuuadwjWSyGlsz26-Wj3-MAdx', '1avuVo0uzDan22O_XKAkxnbVtsSk2XLsSrN_qI2wCfKe_k6rBCxxIppehdJQJiHr'

  #openid
  provider :open_id,  OpenID::Store::Filesystem.new('/tmp')

end

#ActionController::Metal.middleware do
#  use OmniAuth::Strategies::OpenID, OpenID::Store::Filesystem.new('/tmp'), :name => "google",  :identifier => "https://www.google.com/accounts/o8/id"
#end