Causeoftheday::Application.routes.draw do

  match "/organisations/:id" => 'organisations#show', :as => :organisation

  get "payment_response/credit_card"

  get "payment_response/amazon"

  devise_for :organisation_user, :controllers => { :sessions => "organisation_user_sessions", :passwords => "organisation_user_passwords" } do
    get '/login'  => 'organisation_user_sessions#new'
  end

  match 'confirm_amazon_payment' => 'payment_response#amazon'
  match 'confirm_credit_card_payment' => 'payment_response#credit_card'
  match 'confirm_paypal_payment' => 'payment_response#paypal'
  match 'paypal_notification' => 'payment_response#paypal_notification'

  devise_for :donors
  # :path_names => { :sign_up => "register" } do
  #  match '/donor/sign_in/twitter' => Devise::Twitter::Rack::Signin
  #  match '/donor/connect/twitter' => Devise::Twitter::Rack::Connect
  #end

  resources :donations

  resources :search, :only => [] do
    collection do
      get :causes
      get :search_results
    end
  end

  resources :payment_response, :only => [] do
    member do
      get :credit_card
    end
  end

  resources :donors do
    resources :credit_cards
  end

  resources :accounts
  resources :addresses

  resources :causes do
    resources :cause_images
    resources :cause_updates
    member do
      get :stats
    end
  end


  ['admin', 'organisation'].each do |s|
    scope :path => s do
      resources :causes do
        resources :cause_images
        resources :cause_updates
        member do
          get :stats
        end
      end
    end
  end


    resources :causes, :path => '/causes' do
      member do
        get :stats
      end
      match 'action_not_permitted' => 'causes#action_not_permitted', :as => :action_not_permitted
      match 'remove' => 'causes#remove', :as => :remove
      match 'request_approval' => 'causes#request_approval', :as => :request_approval
      resources :cause_images
      resources :cause_updates
    end

  namespace :organisation do
    match 'dashboard' => 'dashboard#index', :as => :dashboard
    match 'terms_and_conditions' => 'dashboard#terms_and_conditions', :as => :terms_and_conditions
    match 'faq' => 'dashboard#faq', :as => :faq
    resources :organisation_users
  end


  namespace :admin do
    match 'dashboard' => 'dashboard#index', :as => :dashboard

    resources :organisations
    resources :admin, :only => [:index] do
      collection do
        get :logout
      end
    end
    resources :schedules do
      collection do
        post :fetch
      end
    end
    resources :donations
  end

  match 'load_cause' => 'home#load_cause', :as => :load_cause
  match 'top_influencer' => 'home#top_influencer', :as => :top_influencer
  match 'facebook_signin' => 'signin_response#facebook', :as => :facebook_signin

  match 'how_it_works' => 'home#how_it_works', :as => :how_it_works
  match 'faq' => 'home#faq', :as => :faq
  match 'contact_us' => 'home#contact_us', :as => :contact_us
  match 'my_donations' => 'donations#my_donations', :as => :my_donations    

  #  match 'signup' => 'donors#new', :as => :signup
  match 'tagged_causes' => 'admin/causes#tagged_causes', :as => :tagged_causes

  match 'admin' => 'admin/admin#index', :as => :admin
  match '/auth/:provider/callback' => 'accounts#create'
  match '/auth/failure' => 'accounts#failure', :as => :failure

  match 'welcome' => 'accounts#welcome', :as => :welcome
  root :to => "home#index"

  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller

  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  # map.root :controller => "welcome"

  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing or commenting them out if you're using named routes and resources.
  #  map.connect ':controller/:action/:id'
  #match ':controller(/:action(/:id))'
  #  map.connect ':controller/:action/:id.:format'
  #match ':controller(/:action(/:id(.:format)))'
end
