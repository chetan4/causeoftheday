set :deploy_to, "/home/ubuntu/apps/97changes-production/"
# Deploy to production site only from stable branch
set :rails_env, "production"

# NOTE: for some reason Capistrano requires you to have both the public and
# # the private key in the same folder, the public key should have the 
# # extension ".pub".

set :user, 'ubuntu'
#set :ssh_options, {:keys => ["#{ENV['HOME']}/.ssh/id_rsa-ec2-rails-keys"]}
set :ssh_options, {:forward_agent => true}

#specifiy the version control system
set :scm, :git
set :repository, "some git repository here...."
set :branch, "production"
#set :deploy_via, :remote_cache
set :use_sudo, false

location = "production-location-here"

role :app, location
role :web, location
role :db, location, :primary => true
