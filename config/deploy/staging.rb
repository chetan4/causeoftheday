set :deploy_to, "/usr/local/apps/causeoftheday/"
set :rails_env, "staging"

set :scm, :git
set :repository, "git@github.com:idyllicsoftware/causeoftheday.git"
set :branch, "master"
#set :deploy_via, :remote_cache

location = "ec2-50-17-222-158.compute-1.amazonaws.com"

role :app, location
role :web, location
role :db, location, :primary => true

set :user, 'ubuntu'
ssh_options[:forward_agent] = true
#set :ssh_options, {:forward_agent => true}

