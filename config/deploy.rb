set :stages, %w(staging production)
set :default_stage, "staging"

require 'capistrano/ext/multistage'
require "bundler/capistrano"

load 'deploy'
load 'deploy/assets'

set :application, "97Changes"

# If you aren't using Subversion to manage your source code, specify
# your SCM below:

namespace :deploy do
  desc "Restarting mod_rails with restart.txt"
  task :restart, :roles => :app, :except => {:no_release => true} do
    run "touch #{current_path}/tmp/restart.txt"

  end

  [:start, :stop].each do |t|
    desc "#{t} task is a no-op with mod_rails"
    task t, :roles => :app do
      ;
    end
  end

  #Avoid keeping the database.yml configuration in git.
  desc "task to create a symlink for the database files."
  task :copy_database_yml, :roles => :app do
      run "ln -s #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end

  desc "task to create a symlink .rvmrc file."
  task :copy_rvmrc, :roles => :app do
      run "ln -s #{shared_path}/.rvmrc #{release_path}/.rvmrc"
  end

end
namespace :delayed_job do
  desc "Start delayed_job process"
  task :start, :roles => :app do
    run "cd #{current_path}; script/delayed_job start #{rails_env}"
  end
  
  desc "Stop delayed_job process"
  task :stop, :roles => :app do
    run "cd #{current_path}; script/delayed_job stop #{rails_env}"
  end

  desc "Restart delayed_job process"
  task :restart, :roles => :app do
    run "cd #{current_path}; RAILS_ENV=#{rails_env} script/delayed_job restart"
  end
end

#run databse.yml copy task.
before "bundle:install", "deploy:copy_rvmrc"
before "bundle:install", "deploy:copy_database_yml"

# DELAYED JOB DISABLING 
#after "deploy:start", "delayed_job:start"
#after "deploy:stop", "delayed_job:stop"
#after "deploy:restart", "delayed_job:restart"

