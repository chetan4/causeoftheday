class IpLocation
  include HTTParty
  format :xml

  attr_reader :ip, :city, :country, :geocodes

  def initialize(response)
    @city = response["HostipLookupResultSet"]["featureMember"]["Hostip"]["name"]
    @country = response["HostipLookupResultSet"]["featureMember"]["Hostip"]["countryName"]
    @geocodes = response["HostipLookupResultSet"]["featureMember"]["Hostip"]["ipLocation"]["pointProperty"]["Point"]["coordinates"] rescue nil
    @ip = response["HostipLookupResultSet"]["featureMember"]["Hostip"]["ip"]
  end

  class << self
    def for(ip)
      new(get(SiteConfig.ip_lookup_url, :query => {:ip => ip}).parsed_response)
    end
  end

end