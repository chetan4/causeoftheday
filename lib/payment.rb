module Payment
  CENTS = 100
  # Used the default e-xact gateway credentials for testing purpose
  EXACT_LOGIN = 'A00049-01'
  EXACT_PASSWORD = 'test1'
  EXACT_TEST = 'test'
  AMOUNT = 1

  class << self
    def process(credit_card, current_donor)
      ActiveMerchant::Billing::Base.mode = :test
      begin
        card = configure_credit_card(credit_card,current_donor)
        p card.errors.full_messages
        p card.valid?
        if card.valid?
          gateway = configure_gateway
          response = gateway.purchase(AMOUNT * CENTS, card)
          if response.success?
            transaction_id = response.authorization
            puts "Successfully charged $#{sprintf("%.2f", AMOUNT / 100)} to the credit card #{credit_card.number}"
          else
            raise StandardError, response.message
          end
        end
      end
      return transaction_id, response 
    end
    
    def configure_credit_card(credit_card, current_donor)
      ActiveMerchant::Billing::CreditCard.new(
        :first_name => current_donor.firstname,
        :last_name => current_donor.lastname,
        :number     => credit_card.number,
        :month      => credit_card.expiration_month,
        :year       => credit_card.expiration_year,
        :verification_value  => credit_card.security_code,
        :type => credit_card.card_type
      )
    end
    
    def configure_gateway
      return ActiveMerchant::Billing::ExactGateway.new(
        :login    => EXACT_LOGIN,
        :password => EXACT_PASSWORD , :test => EXACT_TEST
      )
    end
  end
end

