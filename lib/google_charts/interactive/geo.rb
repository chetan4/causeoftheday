module GoogleCharts
  module Interactive
    class Geo < GoogleCharts::Base
      class << self
          
        # data is supposed to be an array of arrays which first element in the array 
        # being the name of the country and second begin the weight
        # Example: [['India', '400'], ['United States', '220'], ['Russia', 123]]
        # label is supposed to an array with labels in the same order as the data elements.
        # For above example, labels could be [['string', 'Country'], ['number', 'Popularity']]
        def draw(labels, data, div_name, options = {})
          js = include_lib_js
          js += start_script
          js += 
            %^
              google.load('visualization', '1', {'packages': ['geochart']});
              google.setOnLoadCallback(drawRegionsMap);
            ^
          js += function('drawRegionsMap') do
            %^
              var data = new google.visualization.DataTable();
              data.addRows(#{data.size * 2});
              #{
                labels.collect do |datatype_label|
                  %^
                    data.addColumn('#{datatype_label.first}', '#{datatype_label.last}');^
                end.join("")
              }
              #{
                values = []
                data.each_with_index do |name_weight, i|
                  values << %^
                    data.setValue(#{i}, 0, '#{name_weight.first}');
                    data.setValue(#{i}, 1, #{name_weight.last});
                  ^
                end
                values.join('')
              }
              var options = #{options.to_json};

              var container = document.getElementById('#{div_name}');
              var geochart = new google.visualization.GeoChart(container);
              geochart.draw(data, options);
            ^
          end
          
          js += end_script
          
          js
        end
          
      end
    end
  end
end