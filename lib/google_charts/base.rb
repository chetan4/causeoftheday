module GoogleCharts
  class Base
    class << self
      def include_lib_js
        "<script type='text/javascript' src='https://www.google.com/jsapi'></script>"
      end
      
      def start_script
        "<script type='text/javascript'>"
      end
      
      def end_script
        "</script>"
      end
      
      def function(name, *parameters)
        js = "function "
        js += "#{name}("
        js += parameters.join(',') unless parameters.empty?
        js += "){"
        js += yield()
        js += end_function
        js
      end
      
      def end_function
        '}'
      end
      
    end
  end
end