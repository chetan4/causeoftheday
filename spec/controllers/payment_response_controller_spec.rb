require 'spec_helper'

describe PaymentResponseController do

  describe "GET 'credit_card'" do
    before :each do
      @credit_card = Factory(:master_credit_card, :donor => Factory(:credit_card_donor))
      controller.stub(:current_donor).and_return(@credit_card.donor)
    end
    it "should be successful" do
      get 'credit_card', :id => @credit_card.id
      response.should be_true
    end
  end

#  describe "GET 'amazon'" do
#    before :each do
#      controller.stub(:current_donor).and_return(Factory(:credit_card_donor))
#    end
#
#    it "should be successful" do
#      get 'amazon'
#      response.should be_success
#    end
#
#  end

end
