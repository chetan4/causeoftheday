require File.expand_path(File.dirname(__FILE__) + '../../../spec_helper')

describe Admin::OrganisationsController do
  render_views
    
  before :each do
    @help_india_organisation = Factory(:help_india_organisation, :organisation_users => [Factory(:help_india_organisation_user)])
    @teach_india_organisation = Factory(:teach_india_organisation, :organisation_users => [Factory(:teach_india_organisation_user)])

    @girl_child_cause = Factory :girl_child_cause, :organisation => @help_india_organisation
    @help_india_organisation.causes.should include @girl_child_cause
  end

  describe "GET   /admin/organisation/index when user is not logged" do
    it "index action should render access denied message when admin is not logged" do
      get :index
      response.body.should match /Access denied/
    end
  end

  describe "When user is logged" do
    before :each do
      controller.stub(:authenticate_admin).and_return(true)
    end

    describe "GET   /admin/organisation/index" do
      it "index action should list the organisations when admin is logged" do
        get :index
        response.body.should match @help_india_organisation.name
      end
    end

    describe "GET    /admin/organisation/:id" do
      it "show action should give details of a particular organisation" do
        get :show, :id => @help_india_organisation.id
        response.body.should match @help_india_organisation.name
      end
      it "show page must list the causes for an organisation if present" do
        get :show, :id => @help_india_organisation.id
        response.body.should match @girl_child_cause.name
      end
      it "show page must not display any causes if not present" do
        get :show, :id => @teach_india_organisation.id
        response.body.should_not match "Causes under #{@teach_india_organisation.name}"
      end
    end

    describe "GET new organisation /admin/organisation/new" do
      it "new action should render new template to enter new organisation details" do
        get :new
        response.body.should match /Add new Organisation/
        response.body.should match /name/
        response.body.should match /email/
        response.body.should match /website/
        response.body.should match SiteConfig.organisation_type.npo_type
      end
    end

    describe "GET /admin/organisation/:id/edit" do
      it "should render the pre-populated form" do
        get :edit, :id => @help_india_organisation.id
        response.body.should match /Update Organisation/
        response.body.should match SiteConfig.organisation_type.ngo_type
      end
    end

    describe "POST create new organisation - /admin/organisation/:id/create" do
      describe "Valid params" do
        it "should create new organisation" do
          post :create, :organisation => {:name => @teach_india_organisation.name,
            :organisation_type => SiteConfig.organisation_type.ngo_type,
            :website => "http://www.abc.com",
            :organisation_users_attributes => { 0 => {:email => "abc12@gmail.com"}}}

          assigns[:organisation].name.should == @teach_india_organisation.name
          response.should redirect_to(admin_organisation_path(assigns[:organisation]))
        end
      end
      describe "Invalid params" do
        it "should not create new organisation if name, organisation_type is not given" do
          post :create, :organisation => {:name => "",:organisation_type => "" }
          admin_organisation_form_submit(@help_india_organisation.class, :blank_organisation_data, response.body)
        end
        it "should not allow duplicate organisation website" do
          post :create, :organisation => {:website =>  @teach_india_organisation.website }
          admin_organisation_form_submit(@help_india_organisation.class, :duplicate_organisation_data, response.body)
        end
      end
    end

    describe "PUT update the existing organisation - /admin/organisation/:id" do
      before :each do
        @params = {:organisation => {:name => @help_india_organisation.name, :website => @help_india_organisation.website}}
        @invalid_params = {:organisation => {:name => '', :website => '', :organisation_type => ""}}
      end
      describe "Valid params" do
        it 'should update existing organisation' do
          put :update, {:id => @help_india_organisation.id}.merge(@params)
          assigns[:organisation].name.should == @help_india_organisation.name
          response.should redirect_to(admin_organisation_path(assigns[:organisation]))
        end
      end
      describe "Invalid params" do
        it 'should show validation errors if the name, organisation_type is not given' do
          put :update, {:id => @help_india_organisation.id}.merge(@invalid_params)
          response.body.should match /Update Organisation/
          admin_organisation_form_submit(@help_india_organisation.class, :blank_organisation_data, response.body)
        end
      end
    end

    describe "DELETE /admin/organisations/:id" do
      it 'should delete the current organisation' do
        delete 'destroy', :id =>  @help_india_organisation.id
        response.should redirect_to(admin_organisations_path)
      end
    end
  end

  def admin_organisation_form_submit(model, type, response)
    case type
    when :blank_organisation_data
      causeoftheday_messages(model,'name', :blank, response)
      causeoftheday_messages(model,'organisation_type', :blank, response)
    when :duplicate_organisation_data
      causeoftheday_messages(model,'website', :duplicate, response)
    end
  end

end

  
