require File.expand_path(File.dirname(__FILE__) + '../../../spec_helper')

describe Admin::CausesController do
  render_views

  before :each do
    @help_india_organisation = Factory :help_india_organisation
    @girl_child_cause = Factory :girl_child_cause, :organisation => @help_india_organisation
    @help_india_organisation.causes.should include @girl_child_cause
  end

  describe "GET   admin_organisation_causes when user is not logged" do
    it "new action should render access denied message when admin is not logged" do
      get :new, :organisation_id => @help_india_organisation.id
      response.body.should match /Access denied/
    end
  end
  
  describe "when admin is logged in" do

    before :each do
      controller.stub(:authenticate_admin).and_return(true)
    end
    
    describe "GET new cause /admin/causes/new" do
      it "Should add new cause for an organisation" do
        get :new #, :organisation_id => @help_india_organisation.id
        response.body.should match /Create Cause/
        response.body.should match /name/
        response.body.should match /target_amount/
        response.body.should match /description/
        response.body.should match /schedule_date/
      end
    end

    describe "POST create new cause - /admin/causes" do

      describe "Valid parameters" do
        it "should create new cause" do
          post :create, :cause => {:name => 'Sponsor a child', :target_amount => '3000',
            :description => 'Sponsoring a child', :schedule_date => 5.day.since, :organisation_id => @help_india_organisation.id}
          assigns[:cause].name.should ==  "Sponsor a child"
          response.should redirect_to(admin_causes_path)
        end
      end

      describe "Invalid parameters" do

        it "should not create a cause when invalid paramaters are passed" do
          post :create, :cause => {:name => '', :target_amount => '',
            :schedule_date => '', :organisation_id => @help_india_organisation.id}
          admin_cause_form_submit(@help_india_organisation.class, :blank_organisation_data, response.body)
        end

        it "should not allow duplicate cause name" do
          post :create, :organisation_id =>@help_india_organisation.id, :cause => {:name => @girl_child_cause.name, :target_amount => @girl_child_cause.target_amount,
            :description => @girl_child_cause.description,:schedule_date => @girl_child_cause.schedule_date}
          admin_cause_form_submit(@girl_child_cause.class, :duplicate_organisation_data, response.body)
        end

      end
    end

    describe "GET /admin/causes/:id/edit" do
      it "should render the pre-populated form for a cause" do
        get :edit, :id => @girl_child_cause.id
        response.body.should match /Update Cause/
      end
    end

    describe "PUT /admin/causes/:id" do

      before :each do
        @valid_params =  {:cause => {:name => @girl_child_cause.name, :target_amount => @girl_child_cause.target_amount,
            :description => @girl_child_cause.description, :schedule_date => @girl_child_cause.schedule_date, :organisation_id => @help_india_organisation.id }}
        @invalid_params =  {:cause => {:name => '', :target_amount => '',:schedule_date => ''}}
      end

      describe "with valid parameters" do
        it "should update the cause for an organisation" do
          put :update, {:id => @girl_child_cause.id}.merge(@valid_params)
          assigns[:cause].name.should == @girl_child_cause.name
        end
      end

      describe "with invalid parameters" do
        it "should not update the cause incase of invalid paramaters" do
          put :update, {:id => @girl_child_cause.id}.merge(@invalid_params)
          response.body.should match /Update Cause/
          admin_cause_form_submit(@help_india_organisation.class, :blank_organisation_data, response.body)
        end
      end
    end

    describe "DELETE /admin/organisations/:organisation_id/causes/:id" do
      it "Should delete a cause for an organisation" do
        delete 'destroy', {:id => @girl_child_cause.id}
        response.should redirect_to(admin_causes_path)
      end
    end
  end
  
  def admin_cause_form_submit(model, type, response)
    case type
    when :blank_organisation_data
      causeoftheday_messages(model, 'name', :blank, response)
      causeoftheday_messages(model,'target_amount', :blank, response)
    when :duplicate_organisation_data
      #causeoftheday_messages(model, 'scheduled_date', :duplicate, response)
    end
  end
  
end