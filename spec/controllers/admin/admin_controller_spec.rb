require 'spec_helper'

describe Admin::AdminController do
  render_views

  before :each do
    @help_india_organisation = Factory :help_india_organisation
  end

  describe "GET   /admin/organisation/index when user is not logged" do
    it "index action should render access denied message when admin is not logged" do
      get :index
      response.body.should match /Access denied/
    end
  end
  
  describe "When user is logged" do
    before :each do
      controller.stub(:authenticate_admin).and_return(true)
    end
    describe "GET   /admin/organisation/index" do
      it "when the admin is logged in, the index page should have the links for create organisation, list of Organisations and list of Donors" do
        get :index
        response.body.should redirect_to '/admin/dashboard'
      end
    end
  end
end
