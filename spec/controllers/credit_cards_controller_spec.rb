require File.dirname(__FILE__) + '/../spec_helper'

describe CreditCardsController do
  render_views

  it "index action should render index template"
  
  it "show action should render show template"
  
  it "new action should render new template"
  
  it "create action should render new template when model is invalid"

  it "create action should redirect when model is valid"
  
  it "edit action should render edit template"
  
  it "update action should render edit template when model is invalid"

  it "update action should redirect when model is valid"
  
  it "destroy action should destroy model and redirect to index action" 

end
