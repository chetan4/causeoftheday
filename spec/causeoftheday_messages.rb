

def causeoftheday_messages(model, attribute, message_type, response)
  case message_type
  when :blank
    return response.should match "#{model.human_attribute_name(attribute)} can't be blank"
  when :duplicate
    return response.should match "#{model.human_attribute_name(attribute)} has already been taken."
  end
end

