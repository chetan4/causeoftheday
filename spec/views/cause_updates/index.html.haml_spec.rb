require 'spec_helper'

describe "cause_updates/index.html.haml" do
  before(:each) do
    assign(:cause_updates, [
      stub_model(CauseUpdate,
        :title => "Title",
        :description => "MyText"
      ),
      stub_model(CauseUpdate,
        :title => "Title",
        :description => "MyText"
      )
    ])
  end

  it "renders a list of cause_updates" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
