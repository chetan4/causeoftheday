require 'spec_helper'

describe "cause_updates/show.html.haml" do
  before(:each) do
    @cause_update = assign(:cause_update, stub_model(CauseUpdate,
      :title => "Title",
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Title/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/MyText/)
  end
end
