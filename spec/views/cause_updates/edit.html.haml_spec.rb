require 'spec_helper'

describe "cause_updates/edit.html.haml" do
  before(:each) do
    @cause_update = assign(:cause_update, stub_model(CauseUpdate,
      :title => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit cause_update form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => cause_updates_path(@cause_update), :method => "post" do
      assert_select "input#cause_update_title", :name => "cause_update[title]"
      assert_select "textarea#cause_update_description", :name => "cause_update[description]"
    end
  end
end
