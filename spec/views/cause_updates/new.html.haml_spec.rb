require 'spec_helper'

describe "cause_updates/new.html.haml" do
  before(:each) do
    assign(:cause_update, stub_model(CauseUpdate,
      :title => "MyString",
      :description => "MyText"
    ).as_new_record)
  end

  it "renders new cause_update form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => cause_updates_path, :method => "post" do
      assert_select "input#cause_update_title", :name => "cause_update[title]"
      assert_select "textarea#cause_update_description", :name => "cause_update[description]"
    end
  end
end
