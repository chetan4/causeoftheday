require File.expand_path(File.dirname(__FILE__) + '/../spec_helper.rb')

describe Donation do

  context "relations" do
    it { should belong_to :cause }
    it {should belong_to :mode_of_payment, :polymorphic => true}
    it {should belong_to :mode_of_payment_response, :polymorphic => true}
  end

  context "validations" do
    it { should validate_presence_of :amount }
    it { should validate_presence_of :mode_of_payment }
    it { should validate_numericality_of :amount }
  end

  context "columns" do
    it { should have_column :cause_id, :type => :integer }
    it { should have_column :mode_of_payment_id, :type => :integer }
    it { should have_column :mode_of_payment_response_id, :type => :integer }
    it { should have_column :amount, :type => :float}
    it { should have_column :ip_address, :type => :string }
    it { should have_column :mode_of_payment_type, :type => :string }
    it { should have_column :mode_of_payment_response_type, :type => :string }
    it { should have_column :created_at, :type => :datetime }
    it { should have_column :updated_at, :type => :datetime }
  end

end