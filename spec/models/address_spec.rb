require File.expand_path(File.dirname(__FILE__) + '/../spec_helper.rb')

describe Address do

  context "relations" do
    it { should belong_to :owner, :polymorphic => true}
  end
  
  context "validations" do
    it { should validate_presence_of :address_line1 }
    it { should validate_presence_of :city }
    it { should validate_presence_of :state }
    it { should validate_presence_of :country }
    it { should validate_presence_of :zip_code }
    it { should validate_presence_of :owner_id }
    it { should validate_presence_of :owner_type }
  end

  context "columns" do
    it { should have_column :address_line1, :type => :string }
    it { should have_column :address_line2, :type => :string }
    it { should have_column :landline_no, :type => :integer }
    it { should have_column :mobile_no, :type => :integer }
    it { should have_column :city, :type => :string }
    it { should have_column :state, :type => :string }
    it { should have_column :country, :type => :string }
    it { should have_column :zip_code, :type => :integer }
    it { should have_column :owner_id, :type => :integer }
    it { should have_column :owner_type, :type => :string }
    #it { should have_column :address_type_id, :type => :integer }
    it { should have_column :created_at, :type => :datetime }
    it { should have_column :updated_at, :type => :datetime }
  end

  context "display" do
    before(:each) do
#      @help_india_organistaion_user = Factory :help_india_organistaion_user
      @help_india_organisation = Factory(:help_india_organisation, :organisation_users => [Factory(:help_india_organisation_user)])
      @address = Factory :US_home_address, :owner => @help_india_organisation
      #addr type is now removed.
      #@home_address_type = Factory :home_address_type
      #, :address_type => @home_address_type
    end

    it 'should display formatted address in one string for the format html' do
      html_address = @address.format(:html)
      html_address.should match /<p>/
      html_address.should match @address.city
    end
  end

end