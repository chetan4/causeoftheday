require File.dirname(__FILE__) + '/../spec_helper'

describe CreditCard do


  before :each do
    @credit_card_donor = Factory :credit_card_donor
    @master_credit_card = Factory :master_credit_card, :donor => @credit_card_donor
  end


  context "relations" do
    it { should have_one :address, :as => :owner}
    it { should belong_to :donor }
  end

  context "validations" do
    it { should validate_presence_of :donor_id }
    #it { should validate_presence_of :expiration_month }
    #it { should validate_presence_of :expiration_year }
    #it { should validate_presence_of :number }
    #it { should validate_uniqueness_of :number }
  end

  context "columns" do
    it { should have_column :donor_id, :type => :integer }
    it { should have_column :expiration_month, :type => :string }
    it { should have_column :expiration_year, :type => :integer }
    it { should have_column :encrypted_number, :type => :string }
    it { should have_column :created_at, :type => :datetime }
    it { should have_column :updated_at, :type => :datetime }
  end

end
