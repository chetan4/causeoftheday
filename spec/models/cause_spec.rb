require File.expand_path(File.dirname(__FILE__) + '/../spec_helper.rb')

describe Cause do

  context "relations" do
    it { should belong_to :organisation }
    it { should have_many :donations }
  end

  context "validations" do
    it { should validate_presence_of :name }
    it { should validate_presence_of :target_amount }
    it { should validate_presence_of :organisation }
    it { should validate_presence_of :country }
  end

  context "columns" do
    it { should have_column :name, :type => :string }
    it { should have_column :description, :type => :text }
    it { should have_column :organisation_id, :type => :integer }
    it { should have_column :target_amount, :type => :integer }
    it { should have_column :schedule_date, :type => :date }
    it { should have_column :country, :type => :string }
    it { should have_column :city, :type => :string }
    it { should have_column :created_at, :type => :datetime }
    it { should have_column :updated_at, :type => :datetime }
  end

  describe 'todays cause' do
    before :each do
      @organisation = Factory(:help_india_organisation, :organisation_users => [Factory(:help_india_organisation_user)])
      @yesterdays_cause = Factory :aids_awareness_cause, :schedule_date => Date.yesterday, :organisation => @organisation
      @tomorrows_cause = Factory :helpage_india_cause, :schedule_date => Date.tomorrow, :organisation => @organisation
    end

    it 'should fetch the cause scheduled for today' do
      @todays_cause = Factory :girl_child_cause, :schedule_date => Date.today, :organisation => @organisation
      Cause.todays_cause.should == @todays_cause
    end

    #it 'should fetch tomorrows cause if todays cause is complete' do
    #  @todays_cause = Factory :girl_child_cause, :schedule_date => Date.today, :organisation => @organisation,:end_time => Time.now
    #  Cause.todays_cause.id.should == @tomorrows_cause.id
    #end
  end

  describe ': a cause' do
    before :each do
      @organisation = Factory(:help_india_organisation, :organisation_users => [Factory(:help_india_organisation_user)])
      @cause = Factory :child_education_cause, :schedule_date => Date.today, :organisation => @organisation
      @unplanned_cause = Factory :child_education_cause, :organisation => @organisation
      @yesterdays_cause = Factory :girl_child_cause, :schedule_date => Date.yesterday, :organisation => @organisation
      @tomorrows_cause = Factory :helpage_india_cause, :schedule_date => Date.tomorrow, :organisation => @organisation
    end

    describe '.mark_for_review' do

      before :each do
        @unplanned_cause.mark_for_review!
      end

      it 'should update update status of cause' do
        @unplanned_cause.status.should be(Cause::IN_REVIEW)
      end

    end

    describe '.executed?' do
      it 'should return true if it has happened' do
        @yesterdays_cause.executed?.should be_true
      end

      it 'should return false if it is planned for future' do
        @tomorrows_cause.executed?.should be_false
      end

      it 'should return false if its not planned yet' do
        @unplanned_cause.executed?.should be_false
      end
    end

    describe '.scheduled?' do
      it 'should return true if cause is scheduled.' do
        @cause.scheduled?.should be_true
      end

      it 'should return false if cause is not scheduled' do
        @unplanned_cause.scheduled?.should be_false
      end
    end

    describe '.cancel' do
      it 'should cancel the cause' do
        @cause.cancel!
        @cause.cancelled.should be_true
      end

      it 'should save the cause' do
        @cause.cancel!.should be_true
      end
    end

    describe '.now_running?' do
      it 'should return true if cause is scheduled for today.' do
        @cause.now_running?.should be_true
      end

      it 'should return false if cause is not scheduled for today' do
        @unplanned_cause.now_running?.should be_false
      end
    end

    describe '.stage' do
      it 'should return executed if cause is executed' do
        @yesterdays_cause.stage.should be(Cause::EXECUTED)
      end

      it 'should return scheduled if cause is scheduled' do
        @tomorrows_cause.stage.should be(Cause::SCHEDULED)
      end

      it 'should return current status if not scheduled or executed yet.' do
        @unplanned_cause.status = Cause::IN_REVIEW
        @unplanned_cause.stage.should be(Cause::IN_REVIEW)
      end

    end

    describe '.color' do
      it 'should return voilet for executed causes' do
        @yesterdays_cause.color.should be(Cause::COLORS.index('voilet'))
      end

      it 'should return green for running cause' do
        @cause.color.should be(Cause::COLORS.index('green'))
      end

      it 'should return orange for upcomming tasks' do
        @tomorrows_cause.color.should be(Cause::COLORS.index('orange'))
      end
    end

    describe '.total_donations' do
      it 'should return total no. of donations happened.' do
        @cause.stub(:donations).and_return([1,2]) # = Factory :child_education_cause, :schedule_date => Date.today, :organisation => @organisation, :donations => [Factory(:donation_by_credit_card)]
        @cause.total_donation.should be(2)
      end
    end

    describe '.schedule_data' do
      it 'should return array of data result' do
        result = @cause.schedule_data
        result.should == [@cause.id,
                          "#{@cause.name}-(#{@cause.organisation.name})",
                          @cause.schedule_date.strftime("%m/%d/%Y %H:%M"),
                          @cause.schedule_date.strftime("%m/%d/%Y %H:%M"),
                          rand(1),
                          true,
                          0,                                  # 6 Recurring event?
                          @cause.color,                              # 7 Color, -1 to 13.
                          @cause.executed? ? 0 : 1]
      end
    end

    describe '.in_making?' do
      it 'should return true if cause is in making' do
        @unplanned_cause.status = Cause::IN_MAKING
        @unplanned_cause.in_making?.should be_true
      end

      it 'should return false if cause is not in making' do
        @unplanned_cause.status = Cause::IN_REVIEW
        @unplanned_cause.in_making?.should be_false
      end
    end

    describe '.remaining_donations' do
      it 'should return 500 if 500 donations are done.' do
        @cause.stub(:total_donation).and_return(5000)
        @cause.remaining_donations.should == 5000
      end
    end

    describe '.completed_fraction' do
      it 'should return 0.5 if 500 donations are done.' do
        @cause.stub(:total_donation).and_return(5000)
        @cause.completed_fraction.should == 0.5
      end
    end

    describe '.completed_percentage' do
      it 'should return 50% if 500 donations are done.' do
        @cause.stub(:total_donation).and_return(5000)
        @cause.completed_percentage.should == 50
      end
    end

    describe '.donations_with(mode_of_payment)' do
      it 'should return filtered array of donations with given mode_of_payment.' do
        @cause.donations_with('Paypal').should be_kind_of(Array)
      end
    end

    describe '.countriwise_donation_stats' do
      it 'should return array of hash objects with country names and its counts ' do
        result = @cause.countriwise_donation_stats
        result.should be_kind_of(Array)
      end
    end

  end


end