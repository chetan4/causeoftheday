require File.expand_path(File.dirname(__FILE__) + '/../spec_helper.rb')

describe CreditCardResponse do

  context "relations" do
    it { should have_one :donation, :as => :mode_of_payment_response}
  end

  context "columns" do
    it { should have_column :transaction_id, :type => :integer }
    it { should have_column :created_at, :type => :datetime }
    it { should have_column :updated_at, :type => :datetime }
  end
end
