require File.expand_path(File.dirname(__FILE__) + '/../spec_helper.rb')

describe Donor do

  before :each do
    @credit_card_donor = Factory :credit_card_donor
  end

  context "relations" do
    it { should have_many :addresses, :as => :owner}
    it { should have_many :credit_cards }
    it { should have_many  :amazon }
    it { should have_many :authorizations, :dependent => :destroy }
  end

  context "validations" do
    #it { should validate_presence_of :firstname }
    #it { should validate_presence_of :lastname }
    it { should validate_presence_of :username }
    #it { should validate_presence_of :email }
    it { should validate_uniqueness_of :email }
    it { should validate_uniqueness_of :username }
    it { should allow_values_for :email, :message => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i }
  end

  context "columns" do
    it { should have_column :username, :type => :string }
    it { should have_column :email, :type => :string }
    it { should have_column :encrypted_password, :type => :string }
    it { should have_column :password_salt, :type => :string }
    it { should have_column :remember_token, :type => :string }
    it { should have_column :reset_password_token, :type => :string }
    it { should have_column :firstname, :type => :string }
    it { should have_column :middlename, :type => :string }
    it { should have_column :lastname, :type => :string }
    it { should have_column :bdate, :type => :date }
    it { should have_column :created_at, :type => :datetime }
    it { should have_column :updated_at, :type => :datetime }
  end

end