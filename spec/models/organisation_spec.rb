require File.expand_path(File.dirname(__FILE__) + '/../spec_helper.rb')

describe Organisation do

  before :each do
    @help_india_organisation = Factory(:help_india_organisation, :organisation_users => [Factory(:help_india_organisation_user)])
    #, :email => "neha@test.com", :password => "test123", :password_confirmation => "test123"
  end

  context "relations" do
    it { should have_many :addresses, :as => :owner}
    it { should have_many :causes}
  end

  context "validations" do
    it { should validate_presence_of :name }
    #it { should validate_presence_of :email }
    it { should validate_presence_of :organisation_type }
    #it { should validate_presence_of :password }
    #it { should validate_presence_of :username }
    #it { should validate_presence_of :email }
    #it { should validate_uniqueness_of :email }
    #it { should validate_uniqueness_of :username }
    it { should validate_uniqueness_of :website }
    #it { should allow_values_for :email, :message => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i }
  end

  context "columns" do
    it { should have_column :name, :type => :string }
    #it { should have_column :username, :type => :string }
    #it { should have_column :email, :type => :string }
    #it { should have_column :crypted_password, :type => :string }
    #it { should have_column :password_salt, :type => :string }
    #it { should have_column :perishable_token, :type => :string }
    #it { should have_column :persistence_token, :type => :string }
    it { should have_column :organisation_type, :type => :string }
    it { should have_column :website, :type => :string }
    it { should have_column :created_at, :type => :datetime }
    it { should have_column :updated_at, :type => :datetime }
  end
end