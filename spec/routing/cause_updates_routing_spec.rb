require "spec_helper"

describe CauseUpdatesController do
  describe "routing" do

    it "routes to #index" do
      get("/cause_updates").should route_to("cause_updates#index")
    end

    it "routes to #new" do
      get("/cause_updates/new").should route_to("cause_updates#new")
    end

    it "routes to #show" do
      get("/cause_updates/1").should route_to("cause_updates#show", :id => "1")
    end

    it "routes to #edit" do
      get("/cause_updates/1/edit").should route_to("cause_updates#edit", :id => "1")
    end

    it "routes to #create" do
      post("/cause_updates").should route_to("cause_updates#create")
    end

    it "routes to #update" do
      put("/cause_updates/1").should route_to("cause_updates#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/cause_updates/1").should route_to("cause_updates#destroy", :id => "1")
    end

  end
end
