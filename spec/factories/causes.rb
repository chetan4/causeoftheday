Factory.define :girl_child_cause, :class => Cause do |cause|
  cause.name 'Save Girl Child'
  cause.target_amount '5000'
  cause.description 'Please donate for Save Girl Child cause'
  cause.schedule_date 4.day.since
end

Factory.define :blank_cause_with_amount_as_string, :class => Cause do |cause|
  cause.name ''
  cause.target_amount '$5000'
  cause.description 'Please donate for Save Girl Child cause'
  cause.schedule_date 3.day.since
end

Factory.define :aids_awareness_cause, :class => Cause do |cause|
  cause.name 'AIDS Awareness'
  cause.target_amount '15000'
  cause.description 'Lets create awareness for AIDS'
  cause.schedule_date 2.day.since
end

Factory.define :aids_awareness_cause_with_blank_schedule_date_and_amount_as_string, :class => Cause do |cause|
  cause.name 'AIDS Awareness'
  cause.target_amount '$15000'
  cause.description 'Lets create awareness for AIDS'
  cause.schedule_date ''
end

Factory.define :helpage_india_cause, :class => Cause do |cause|
  cause.name 'Helpage India'
  cause.target_amount '10000'
  cause.description 'Lets join our hands for blind people and help them'
  cause.schedule_date 1.day.since
end

Factory.define :child_education_cause, :class => Cause do |cause|
  cause.name 'Helpage India'
  cause.target_amount '10000'
  cause.description 'Lets join our hands for blind people and help them'
end