
Factory.define :amazon_responses, :class => AmazonResponse do |amazon_response|
  amazon_response.caller_reference ''
  amazon_response.transaction_id '12334345'
end

Factory.define :blank_transaction_id_amazon_responses, :class => AmazonResponse do |amazon_response|
  amazon_response.transaction_id ''
end
