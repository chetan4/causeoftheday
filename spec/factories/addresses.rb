
Factory.define :US_home_address, :class => Address do |address|
  address.address_line1 '13625 S48th ST'
  address.address_line2 'Sonoran Apartments Apt#1210'
  address.landline_no '480-961-2206'
  address.mobile_no '605-535-9140'
  address.city 'Phoenix'
  address.state 'Arizona'
  address.country 'USA'
  address.zip_code '85044'
  address.address_type 'home'
end

Factory.define :US_home_address_with_blank_address_lines_and_zip_code, :class => Address do |address|
  address.address_line1 ''
  address.address_line2 ''
  address.landline_no '480-961-2206'
  address.mobile_no '605-535-9140'
  address.city 'Phoenix'
  address.state 'Arizona'
  address.country 'USA'
  address.zip_code ''
  address.address_type 'home'
end

Factory.define :US_office_address, :class => Address do |address|
  address.address_line1 '13630 S48th ST'
  address.address_line2 'San Melia Apartments Apt#1235'
  address.landline_no '111-222-2206'
  address.mobile_no '333-444-9140'
  address.city 'Phoenix'
  address.state 'Arizona'
  address.country 'USA'
  address.zip_code '85044'
  address.address_type 'office'
end

Factory.define :US_office_address_with_blank_address_lines_and_zip_code, :class => Address do |address|
  address.address_line1 ''
  address.address_line2 ''
  address.landline_no '111-222-2206'
  address.mobile_no '333-444-9140'
  address.city 'Phoenix'
  address.state 'Arizona'
  address.country 'USA'
  address.zip_code ''
  address.address_type 'office'
end

Factory.define :India_home_address, :class => Address do |address|
  address.address_line1 'Ginni Bldg, Gurunanak Rd'
  address.address_line2 'Dehuroad'
  address.landline_no '480-961-2206'
  address.mobile_no '605-535-9140'
  address.city 'Pune'
  address.state 'Maharashtra'
  address.country 'India'
  address.zip_code '412101'
  address.address_type 'home'
end

Factory.define :India_home_address_with_blank_address_lines_and_zip_code, :class => Address do |address|
  address.address_line1 ''
  address.address_line2 ''
  address.landline_no '480-961-2206'
  address.mobile_no '605-535-9140'
  address.city 'Pune'
  address.state 'Maharashtra'
  address.country 'India'
  address.zip_code ''
  address.address_type 'home'
end

Factory.define :India_office_address, :class => Address do |address|
  address.address_line1 '106 East, Main Bazar,'
  address.address_line2 'Ratan Apart. Flat # 501'
  address.landline_no '111-222-2206'
  address.mobile_no '333-444-9140'
  address.city 'Pune'
  address.state 'Maharashtra'
  address.country 'India'
  address.zip_code '412115'
  address.address_type 'home'
end

Factory.define :India_office_address_with_blank_state_and_country, :class => Address do |address|
  address.address_line1 '106 East, Main Bazar,'
  address.address_line2 'Ratan Apart. Flat # 501'
  address.landline_no '111-222-2206'
  address.mobile_no '333-444-9140'
  address.city 'Pune'
  address.state ''
  address.country ''
  address.zip_code '425889'
  address.address_type 'office'
end

