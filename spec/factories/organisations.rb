Factory.define :help_india_organisation, :class => Organisation do |organisation|
  organisation.name "Helpage India"
  organisation.organisation_type 'NGO'
  organisation.website 'http://www.HelpageIndia.com'
end

Factory.define :blank_name_organisation, :class => Organisation do |organisation|
  organisation.name ""
  organisation.email "help_india@ngo.com"
end

Factory.define :teach_india_organisation, :class => Organisation do |organisation|
  organisation.name "Teach India"
  organisation.organisation_type 'NPO'
  organisation.website 'http://www.TeachIndia.com'

end

Factory.define :blank_email_organisation, :class => Organisation do |organisation|
  organisation.name "Teach India"
  organisation.email ""
end