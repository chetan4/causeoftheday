Factory.define :credit_card_responses, :class => CreditCardResponse do |credit_card_response|
  credit_card_response.transaction_id '12334345'
end

Factory.define :blank_transaction_id_credit_card_responses, :class => CreditCardResponse do |credit_card_response|
  credit_card_response.transaction_id ''
end
