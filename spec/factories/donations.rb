Factory.define :donation_by_credit_card, :class => Donation do |donation|
  donation.amount "100"
  donation.ip_address "192.168.1.1"
end

Factory.define :donation_by_credit_card_with_amount_as_string, :class => Donation do |donation|
  donation.amount "$100"
  donation.mode_of_payment "Credit card"
end

Factory.define :donation_by_amazon, :class => Donation do |donation|
  donation.amount "100"
  donation.mode_of_payment "Amazon"
  donation.mode_of_payment_response "Amazon"
end

Factory.define :donation_by_amazon_with_amount_as_string, :class => Donation do |donation|
  donation.amount "$100"
  donation.mode_of_payment "Amazon"
end

Factory.define :donation_with_blank_mode_of_payment, :class => Donation do |donation|
  donation.amount "100"
  donation.mode_of_payment ""
end
