
Factory.define :master_credit_card, :class => CreditCard do |credit_card|
  credit_card.expiration_month 5.months.since(Time.now)
  credit_card.expiration_year 5.years.since(Time.now)
  credit_card.number '4444-5555-9999-8888'
  credit_card.security_code  '123'
end

Factory.define :visa_credit_card, :class => CreditCard do |credit_card|
  credit_card.expiration_month 9.months.since(Time.now)
  credit_card.expiration_year 15.years.since(Time.now)
  credit_card.number '1234-5555-9999-8888'
  credit_card.security_code  '123'
end

Factory.define :master_credit_card_with_blank_card_no, :class => CreditCard do |credit_card|
  credit_card.expiration_month 2.months.since(Time.now)
  credit_card.expiration_year 9.years.since(Time.now)
  credit_card.number ''
  credit_card.security_code  '123'
end

Factory.define :master_credit_card_with_no_expiry_date_entry, :class => CreditCard do |credit_card|
  credit_card.number '4444-5555-9999-8888'
  credit_card.security_code  '123'
end