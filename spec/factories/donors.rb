Factory.define :credit_card_donor, :class => Donor do |donor|
  donor.email 'donor@test.com'
  donor.username 'testdonor'
  donor.firstname 'Test'
  donor.lastname 'Donor'
  donor.password 'passme'
  donor.password_confirmation 'passme'
end

Factory.define :blank_email_credit_card_donor, :class => Donor do |donor|
  donor.email ''
  donor.username 'testdonor'
  donor.firstname 'Test'
  donor.lastname 'Donor'
end

Factory.define :amazon_donor, :class => Donor do |donor|
  donor.email 'amazon_donor@test.com'
  donor.username 'amazon_testdonor'
  donor.firstname 'Amazon'
  donor.lastname 'Test'
  donor.password 'passme'
  donor.password_confirmation 'passme'  
end

Factory.define :blank_firstname_and_email_amazon_donor, :class => Donor do |donor|
  donor.email ''
  donor.username 'amazon_testdonor'
  donor.firstname ''
  donor.lastname 'Test'
end
