class Organisation < ActiveRecord::Base

  acts_as_taggable

  has_many :causes, :dependent => :destroy
  has_many :addresses, :as => :owner, :dependent => :destroy
  has_many :organisation_users, :dependent => :destroy
  validates :name, :organisation_type,  :presence => true

  accepts_nested_attributes_for :organisation_users

  validates_uniqueness_of :website, :allow_blank => true

  validates_format_of :website, :with => /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$/ix, :allow_blank => true

  #validates_attachment_content_type :logo, :content_type => ['image/jpeg', 'image/png', 'image/jpg']

  before_validation(:on => :create) do
    generate_password_for_user
  end

  has_attached_file :logo

  define_index do
    indexes name, :sortable => true
    indexes organisation_type
    indexes website
  end

  def generate_password_for_user
    self.organisation_users.first.generate_password rescue nil
  end

  def to_param
    "#{id}-#{name.parameterize}"
  end

end
