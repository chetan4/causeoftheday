class TwitterAccount < Account

  class << self

    def create(omniauth, donor)
      create!(:provider           => omniauth['provider'],
              :uid                => omniauth['uid'],
              :donor              => donor,
              :access_token       => omniauth['credentials']['token'],
              :secret             => omniauth['credentials']['secret'],
              :profile_image_url  => omniauth['user_info']['image'],
              :user_info          => Marshal.dump(omniauth['user_info'])
      )
    end

  end

  def update_tokens!(omniauth)
    update_attributes(:access_token => omniauth['credentials']['token'], :secret => omniauth['credentials']['secret'])
  end

  def publish(donation)
    TwitterService.broadcast(self, donation)  unless Rails.env.development?
  end

end

