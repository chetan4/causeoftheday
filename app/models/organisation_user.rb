class OrganisationUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable, :lockable and :timeoutable
  attr_accessor :generated_password
  devise :database_authenticatable,
    :recoverable, :rememberable, :trackable, :validatable

  belongs_to :organisation

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :last_name, :first_name

  #acts_as_authentic do |c|
  #  c.login_field = :email
  #  c.merge_validates_format_of_email_field_options :message => "missing", :allow_blank => true, :if => Proc.new {|obj| obj.email.blank?}
  #  c.merge_validates_length_of_email_field_options :message => "", :allow_blank => true, :allow_nil => true
  #  c.validates_length_of_password_field_options  = { :minimum => 0, :allow_blank => true, :if => :require_password? }
  #  c.ignore_blank_passwords = true
  #end

  after_create :deliver_notification

  def name
    "#{first_name} #{last_name}"
  end

  def deliver_notification
    OrganisationUserMailer.welcome_email(self,self.generated_password).deliver rescue nil
  end

  def generate_password
    return if self.password.present?
    random_password = (0...8).map{ ('a'..'z').to_a[rand(26)] }.join
    self.generated_password = random_password
    self.password = random_password
    self.password_confirmation = random_password
  end

end
