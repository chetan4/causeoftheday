class Cause < ActiveRecord::Base

  acts_as_taggable

  belongs_to :organisation
  delegate :name, :to => :organisation, :prefix => true

  has_many :donations, :dependent => :destroy 

  has_many :images, :class_name => "CauseImage", :dependent => :destroy
  has_many :updates, :class_name => "CauseUpdate", :dependent => :destroy

  has_many :donor_connections
  has_many :updates, :class_name => "CauseUpdate", :dependent => :destroy
  #validates_uniqueness_of :schedule_date, :allow_blank => true, :allow_nil => true

  validates_numericality_of :target_amount, :allow_nil => true
  validates :name, :organisation, :country, :presence => true
  validates :target_amount, :presence => true, :unless => :permanent?

  COLORS = ['redish pink', 'red', 'pink', 'dark pink', 'voilet', 'blue', 'light blue', 'light green', 'green', 'bright green', 'parrot green', 'yellow green', 'occure yellow', 'orange', 'maroon', 'greyish maroon']

  IN_MAKING = "in_making"
  IN_REVIEW = "in_review"
  SCHEDULED = "scheduled"
  EXECUTED = "executed"
  PERMANENT = "permanent"

  scope :in_review,         where(:status     => IN_REVIEW)
  scope :in_making,        where(:status     => IN_MAKING)
  scope :scheduled,        where('schedule_date IS NOT ?', nil)
  scope :executed,         where('schedule_date != ? and schedule_date > ?', nil, Time.now)
  scope :not_cancelled,   where(:cancelled  => false)
  scope :permanent,       where(:permanent  => true).limit(1)
  scope :for_organisation,          lambda {|organisation| where(:organisation => organisation)}
  scope :filter_by_status,           lambda {|status|       where(:status       => status) if status}
  scope :filter_by_organisation, lambda { |organisation_user|
    where(:organisation_id => organisation_user.organisation_id ) if organisation_user
  }
  scope :running_today,   lambda{|date|
    where('? between start_date and end_date', date)
  }

  before_save :set_status_as_scheduled

  after_create :notify_admin

  define_index do
    indexes name, :sortable => true
    indexes description
  end

  class << self

    def todays_cause(offset=0)
      Cause.running_today(today(offset)).first || permanent.first
    end

    def today(offset=0)
      (Time.now + offset.minutes).to_date
    end

  end

  def to_param; "#{id}-#{name.parameterize}";  end

  def now_running?(offset=0)
    Cause.today(offset).between?(start_date, end_date)
  end

  def cancel!
    update_attribute :cancelled, true
  end

  def mark_for_review!
    update_attribute :status, IN_REVIEW
  end

  def stage(offset=0)
    if permanent?
      return PERMANENT
    elsif executed?(offset)
      return EXECUTED
    elsif scheduled?
      return SCHEDULED
    else
      self.status
    end
  end

  def donation_amount_collected; donations.sum(:amount); end
  def number_of_donations; donations.count; end
  def expected_payout; 0 end

  def schedule_data
    [
        id,
        "#{name}-(#{organisation.name})",
        schedule_date.strftime("%m/%d/%Y %H:%M"),
        rand(1),
        true,
        0,                                  # 6 Recurring event?
        color,                              # 7 Color, -1 to 13.
        executed? ? 0 : 1
    ]
  end

  def remaining_donation_amount
    target_amount - donation_amount_collected
  end

  def completed_amount_percentage
    completed_amount_fraction * 100
  end

  def completed_amount_fraction
    fraction = donation_amount_collected.to_f / target_amount.to_f
    fraction.infinite? ? 0.5 : fraction 
  end

  def geo_graph_data
    donations.group('country').size.to_a
  end

  def geo_graph_labels
    {
        'string' => 'Country',
        'number' => 'Donations'
    }.to_a
  end

  private

  def executed?(offset=0)
    scheduled? && (Cause.today(offset) > end_date)
  end

  def scheduled?
    start_date.present? and end_date.present?
  end

  def in_making?(offset=0)
    self.stage(offset) == IN_MAKING
  end

  def set_status_as_scheduled(offset=0)
    if scheduled?
      self.stage(offset) == SCHEDULED
    end
  end

  def color(offset=0)
    if executed?(offset)
      COLORS.index('voilet')
    elsif now_running?(offset)
      COLORS.index('green')
    else
      COLORS.index('orange')
    end
  end

  def permanent?
    permanent
  end

  def notify_admin
    CauseMailer.notify_admin(self).deliver
  end



end
