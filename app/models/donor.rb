class Donor < ActiveRecord::Base

  # To use devise-twitter don't forget to include the :twitter_oauth module:
  # e.g. devise :database_authenticatable, ... , :twitter_oauth

  # IMPORTANT: If you want to support sign in via twitter you MUST remove the
  #            :validatable module, otherwise the user will never be saved
  #            since it's email and password is blank.
  #            :validatable checks only email and password so it's safe to remove

  # Include default devise modules. Others available are:
  # :token_authenticatable, :lockable, :timeoutable and :activatable
  #  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable
  #, :twitter_oauth

  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable

  validates_presence_of :username
  validates_uniqueness_of :email

  has_many :accounts, :dependent => :destroy do
    def broadcast(donation)
      each do |provider|
        provider.publish(donation)
      end
    end
  end

  has_many :donations
  has_many :amazon
  has_many :paypal
  has_many :credit_cards
  has_many :amazon_donations, :through => :amazon, :source => :donations
  has_many :paypal_donations, :through => :paypal, :source => :donations
  has_many :credit_card_donations, :through => :credit_cards, :source => :donations

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation,  :middlename, :lastname, :bdate, :firstname, :username, :remember_me
  has_many :addresses, :as => :owner
  has_many :donor_connections

  before_create :generate_uuid

  def timezone_object
    ActiveSupport::TimeZone[timezone]
  end

  class << self

    def create_with_omniauth(omniauth)
      omniauth = HashWithIndifferentAccess.new(omniauth)
      create!(:username => omniauth['user_info'][:name].downcase.scan(/[a-zA-Z0-9_]/).join(""),
              :email => omniauth['user_info'][:email],
              :firstname => omniauth['user_info'][:first_name],
              :lastname => omniauth['user_info'][:last_name]
              )
    end

  end

  def donated?(cause)
    !(amazon_donations.find_by_cause_id(cause.id) || paypal_donations.find_by_cause_id(cause.id)).nil?
  end

  def full_name
    "#{self.firstname} #{self.lastname}"
  end

  def broadcast(donation)
    accounts.broadcast(donation)
  end

  private

  def generate_uuid
    self.uuid = UUID.new.generate
  end
end