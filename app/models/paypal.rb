class Paypal < ActiveRecord::Base
  has_many :donations, :as => :mode_of_payment
  has_many :responses, :through => :donations, :class_name => 'PaypalResponse'
  belongs_to :donor
end
