class CauseUpdate < ActiveRecord::Base

  belongs_to :cause

  validates :title, :description, :presence => true
end
