class Donation < ActiveRecord::Base

  belongs_to :cause
  belongs_to :donor
  belongs_to :mode_of_payment, :polymorphic => true
  belongs_to :mode_of_payment_response, :polymorphic => true

  validates_associated :cause
  validates :amount, :mode_of_payment, :mode_of_payment_response, :presence => true
  validates_numericality_of :amount

  after_create :set_location, :broadcast

  scope :donated_via, lambda { |mode_of_payment| where(:mode_of_payment_type => mode_of_payment)}

  class << self

    def build_from_response(type, user, params)
       response_klass = "#{type.to_s.camelcase}Response".constantize
       donation_attributes = response_klass.build_donation_attributes(user, params)
       Donation.new donation_attributes
    end

  end

  def amount=(value)
    super(value.scan(/\d+/).first)
  end

  private

  def set_location
    location = IpLocation.for(ip)
    self.country = location.country
    self.city = location.city
    self.save!
  end

  def broadcast
    mode_of_payment.donor.broadcast(self)
  end

end

