class CauseImage < ActiveRecord::Base
  belongs_to :cause
  
  has_attached_file :image, :styles => {:thumb  => "50x50>",
                                        :small  => "150x150>",
                                        :medium => "300x200>",
                                        :large  => "600x600>"}

  validates_attachment_presence :image
  validates_attachment_size :image, :less_than => 5.megabytes
  validates_attachment_content_type :image, :content_type => ['image/jpeg', 'image/jpg', 'image/png', 'image/gif']

end
