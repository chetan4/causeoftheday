class Address < ActiveRecord::Base
  belongs_to :owner, :polymorphic => true

  attr_accessible :address_line1,:address_line2, :landline_no, :mobile_no, :city, :country, :zip_code, :owner_id, :owner_type, :state, :address_type

  validates :address_line1, :city, :state, :country, :zip_code, :owner_id, :owner_type, :address_type, :presence => true

  def format(type)
    case type
    when :html
      "<p>City: #{city}</p>"
    end
  end
end
