class Account < ActiveRecord::Base

  belongs_to :donor

  validates :donor_id, :uid, :provider, :presence => true
  validates_uniqueness_of :uid, :scope => :provider

  class << self

    def authorize(omniauth, donor)
      create_or_update_account(omniauth, donor)
    end

    private

    def create_or_update_account(omniauth, donor)
      account = find_by_provider_and_uid(omniauth['provider'], omniauth['uid']) || create(omniauth, donor)
      account.update_tokens!(omniauth)
      account
    end

    def create(omniauth, donor)
      donor = donor || Donor.create_with_omniauth(omniauth)
      provider_class(omniauth['provider']).create(omniauth, donor)
    end

    def provider_class(provider)
      "#{provider.camelcase}Account".constantize
    end

  end

end



















#def authorize(omniauth, donor)
      #account = find_by_provider_and_uid(omniauth['provider'], omniauth['uid'])
      #
      #if account
      #  account.update_tokens!(omniauth)
      #  return account
      #
      #elsif donor
      #  account = provider_class(omniauth['provider']).create_account!(omniauth, donor)
      #
      #else
      #  donor = Donor.new
      #  donor.create_with_omniauth!(omniauth)
      #  account = provider_class(omniauth['provider']).create_account!(omniauth, donor)
      #end
      #account
    #end

