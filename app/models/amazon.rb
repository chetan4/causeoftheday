class Amazon < ActiveRecord::Base
  has_many :donations, :as => :mode_of_payment
  has_many :responses, :through => :donations, :class_name => 'AmazonResponse'
  belongs_to :donor
end
