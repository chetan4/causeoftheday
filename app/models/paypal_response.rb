class PaypalResponse < ActiveRecord::Base

  class << self

    def build_donation_attributes(user, params)
      paypal    = Paypal.find_or_create_by_donor_id_and_email(user.id, params[:payer_email])
      paypal_response = PaypalResponse.create_response(params)
      {
          :mode_of_payment          => paypal,
          :mode_of_payment_response => paypal_response,
          :cause                    => Cause.find(params[:cause_id]),
          :amount                   => paypal_response.payment_gross.to_s,
          :donor_id                 => user.id
      }
    end

    def create_response(hash)
      paypal_response = prepare_response(hash)
      paypal_response.save!
      paypal_response
    end

    private

    def prepare_response(hash)
      response = PaypalResponse.new
      response.payment_status = hash[:payment_status]
      response.tax = hash[:tax]
      response.receiver_email = hash[:receiver_email]
      response.receiver_id = hash[:receiver_id]
      response.quantity = hash[:quantity]
      response.business = hash[:business]
      response.payment_gross = hash[:payment_gross]
      response.transaction_subject = hash[:transaction_subject]
      response.pending_reason = hash[:pending_reason]
      response.notify_version = hash[:notify_version]
      response.payment_fee = hash[:payment_fee]
      response.mc_currency = hash[:mc_currency]
      response.txn_id = hash[:txn_id]
      response.verify_sign = hash[:verify_sign]
      response.item_name = hash[:item_name]
      response.test_ipn = hash[:test_ipn]
      response.txn_type = hash[:txn_type]
      response.charset = hash[:charset]
      response.mc_gross = hash[:mc_gross]
      response.mc_fee = hash[:mc_fee]
      response.payer_id = hash[:payer_id]
      response.last_name = hash[:last_name]
      response.custom = hash[:custom]
      response.payer_status = hash[:payer_status]
      response.payer_email = hash[:payer_email]
      response.protection_eligibility = hash[:protection_eligibilit]
      response.residence_country = hash[:residence_country]
      response.payment_date = hash[:payment_date]
      response.item_number = hash[:item_number]
      response.first_name = hash[:first_name]
      response
    end
  end
end
