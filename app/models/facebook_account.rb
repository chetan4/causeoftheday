require 'httparty'
class FacebookAccount < Account
  include HTTParty

  belongs_to :donor

  FACEBOOK_BASE_URL = 'https://graph.facebook.com'

  class << self

    def create(omniauth, donor)
      create!(:provider       => omniauth['provider'],
              :uid           => omniauth['uid'],
              :donor         => donor,
              :access_token  => omniauth['credentials']['token'],
              :secret        => omniauth['credentials']['secret'],
              :profile_image_url  => omniauth['user_info']['image'],
              :user_info     => Marshal.dump(omniauth['user_info'])
      )
    end

  end

  def update_tokens!(omniauth)
    update_attribute :access_token, omniauth['credentials']['token']
  end

  def publish(donation)
    post_to_facebook(donation)
    donation.update_attributes(:broadcasted => true)
  end

  private

  def post_to_facebook(donation)
    client = FBGraph::Client.new(:client_id => uid ,:token => access_token)
    u = client.selection.me.info!
    client.selection.user(u[:id]).feed.publish!(
        :message => "I donated for #{donation.cause.name} on 97 Changes. Feels good and satisfied to be helpful to somebody in need.",
        :name => "Donate Now !!",
        :link => "#{SiteConfig.host}?uuid=#{donor.uuid}",
        :picture => 'https://d3nwyuy0nl342s.cloudfront.net/images/modules/header/logov3-hover.png',
        :description => donation.cause.description
    )
  end

end
