 
class CreditCard < ActiveRecord::Base
  
  has_one :address, :as => :owner, :dependent => :destroy
  has_many :donations, :as => :mode_of_payment
  has_many :responses, :through => :donations, :class_name => 'CreditCardResponse'
  belongs_to :donor
  validate :number, :security_code
  
  validates_presence_of :donor_id

  validates :expiration_month,:expiration_year,:presence => true
  attr_encrypted :number, :key => ::ENCRYPTED_KEY_FOR_CREDIT_CARD
  attr_encrypted :security_code, :key => ::ENCRYPTED_KEY_FOR_SECURITY_CODE

  before_validation(:on => :create) do
    self.number = number.gsub(/[^0-9]/, "") if attribute_present?("number")
  end

  validate do |credit_card|
    if credit_card.new_record?
      credit_card.errors[:base] << "Number is required" if credit_card.encrypted_number.blank?
      credit_card.errors[:base] << "Security Code is required" if credit_card.encrypted_security_code.blank?
    elsif !(!credit_card.new_record? && credit_card.number.blank? && credit_card.security_code.blank?)
      credit_card.errors[:base] << "Security Code must be 3 .. 4 characters" if (credit_card.security_code.length < 3 || credit_card.security_code.length > 4)
      credit_card.errors[:base] << "Number must be 16 .. 17 characters" if (credit_card.number.length < 16 || credit_card.number.length > 17)
    end
  end
  
end