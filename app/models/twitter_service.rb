require('twitter')
class TwitterService
  class << self

    def authenticate(account)
      Twitter.configure do |config|
        config.consumer_key = SiteConfig.twitter.consumer_key
        config.consumer_secret = SiteConfig.twitter.consumer_secret
        config.oauth_token = account.access_token
        config.oauth_token_secret = account.secret
      end
    end

    def broadcast(account, donation)
      authenticate(account)
      client = Twitter::Client.new
      client.update("I donated for #{donation.cause.name} on 97 Changes. Check - #{SiteConfig.host}?uuid=>#{account.donor.uuid}")
    end

  end
end
