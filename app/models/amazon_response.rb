class AmazonResponse < ActiveRecord::Base
  has_one :donation, :as => :mode_of_payment_response
  validates :transaction_id,:presence => true

  class << self

    def build_donation_attributes(user, params)
      amazon = Amazon.find_or_create_by_donor_id_and_email(user.id, params[:buyerEmail])
      {
          :mode_of_payment          => amazon,
          :mode_of_payment_response => create_response(params),
          :cause                    => Cause.find(params[:paymentReason].to_i),
          :amount                   => params[:transactionAmount],
          :user_id                  => user.id
      }
    end

    def create_response(params)
      response = prepare_response(params)
      response.save!
      response
    end

    private

    def prepare_response response
      amazon_payment_response = AmazonResponse.new
      amazon_payment_response.payment_reason =  response[:paymentReason]
      amazon_payment_response.buyer_name = response[:buyerName]
      amazon_payment_response.signature_version = response[:signatureVersion]
      amazon_payment_response.transaction_date = response[:transactionDate]
      amazon_payment_response.certificate_url  = response[:certificateUrl]
      amazon_payment_response.recipient_name  = response[:recipientName]
      amazon_payment_response.signature  = response[:signature]
      amazon_payment_response.recipient_email  = response[:recipientEmail]
      amazon_payment_response.signature_method  = response[:signatureMethod]
      amazon_payment_response.transaction_id  = response[:transactionId]
      amazon_payment_response.reference_id  = response[:referenceId]
      amazon_payment_response.payment_method  = response[:paymentMethod]
      amazon_payment_response.operation  = response[:operation]
      amazon_payment_response.status = response[:status]
      amazon_payment_response
    end
  end
end
