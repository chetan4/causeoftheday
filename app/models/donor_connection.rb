class DonorConnection < ActiveRecord::Base
  belongs_to :cause
  belongs_to :donor, :class_name => "Donor"
  belongs_to :influencer, :class_name => "Donor", :foreign_key => "influencer_id"

  def self.top_influencer
    begin
      todays_cause_id = Cause.todays_cause.id
      top_influencer_donations = DonorConnection.select('COUNT(DISTINCT(donor_connections.id)) AS influencer_donations_count, donor_connections.*').where(:cause_id => todays_cause_id).group('donor_connections.influencer_id').order('influencer_donations_count DESC').limit(1).first
      top_influencer_donations['influencer_donations_count']
      return Donor.find(top_influencer_donations['influencer_id'])
    rescue
      return nil
    end
  end
end
