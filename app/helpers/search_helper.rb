module SearchHelper

  def find_causes(search_string, options = {})
    return Cause.search search_string, options
  end

  def find_donors
  end

  private

  def find_city
  end

  def find_state
  end

  def find_country
  end

end
