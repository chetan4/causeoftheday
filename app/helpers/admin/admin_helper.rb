module Admin::AdminHelper
  include ActsAsTaggableOn::TagsHelper

  def rhs_box(locals)
    render :partial => '/layouts/admin/rhs_box', :locals => locals
  end
end
