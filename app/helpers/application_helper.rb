# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  
  def under_admin?
    params[:controller].match('admin')
  end

  def search_result?
    params[:controller].match('search')
  end

  def search_result_heading(query)
    "Search results for #{query}"
  end

  def check_active(value)
    value == request.path ? 'active' : ''
  end

  def check_controller_active(value)
    value.split(" ").include?(controller.class.to_s) ? 'active' : ''
  end

  def permanent_cause_path
    cause_path(Cause.permanent.first)
  end

  def time_format(time)
    time.strftime("%d/%m/%y %H:%m %Z")
  end
end
