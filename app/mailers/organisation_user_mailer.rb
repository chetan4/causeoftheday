class OrganisationUserMailer < ActionMailer::Base
  default :from => "97changes@gmail.com"

  def welcome_email(user,password)
    @user = user
    @password = password
    @url  = "#{SiteConfig.host}/organisation_user/sign_in"
    mail(:to => user.email,
      :subject => "Welcome to 97Changes")
  end

end
