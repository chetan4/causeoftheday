class CauseMailer < ActionMailer::Base
  default :from => "97changes@gmail.com"

  def notify_admin(cause)
    @cause = cause
    mail(:to => SiteConfig.admin.email,
      :subject => "New cause : #{@cause.name}")
  end

end
