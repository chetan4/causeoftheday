class HomeController < ApplicationController

  before_filter :load_uuid#, :fetch_friends, :fetch_profile_image

  def index
    if current_organisation_user
      redirect_to organisation_dashboard_path
    else
      @cause = Cause.todays_cause(params[:offset_minutes].to_i)
      render :template => '/home/index'
    end
  end

  def load_cause
    @cause = Cause.todays_cause(params[:offset_minutes].to_i)
    session[:cause_id] = @cause.id if @cause
    render :partial => '/home/cause', :locals => {:cause => @cause}
  end

  def top_influencer
    @top_influencer = DonorConnection.top_influencer
    if @top_influencer
      session[:top_influencer] = @top_influencer
      render :partial => '/home/top_influencer'
    else
      render :nothing => true
    end
  end

  def how_it_works

  end

  def faq

  end

  def contact_us

  end

  private

  def load_uuid
    if params[:uuid]
      session[:uuid] = params[:uuid]
    end
  end

  def fetch_friends
    if current_donor && current_donor.twitter_user?
      session[:total_friends] = TwitterService.fetch_friends current_donor
    end
  end

  def fetch_profile_image
    if session[:top_influencer] && session[:top_influencer].twitter_user?
      session[:profile_picture] = TwitterService.profile_image session[:top_influencer]
    elsif session[:top_influencer] && session[:top_influencer].facebook_user?
      session[:profile_picture] = FacebookAccount.profile_image session[:top_influencer]
    end
  end
end
