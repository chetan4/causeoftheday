class PaymentResponseController < ApplicationController
  skip_before_filter :verify_authenticity_token


  def amazon
    @donation = Donation.build_from_response('amazon', user, params)
    if @donation.save!
      flash[:notice] = "Donation succesfully accepted"
      redirect_to donations_path(:mode_of_payment => 'amazon_donations')
    else
      flash[:notice] = "Sorry Donation could not be accepted."
      redirect_to root_url
    end
  end

  def paypal
    @donation = Donation.build_from_response('paypal', current_donor, params)
    @donation.ip = request.ip
    @donation.save!(:validate => false)
    redirect_to root_url
  end

  def paypal_notification
    p '#' * 100
    p params.inspect
    p '#' * 100
    render :nothing => true
  end


  #TODO credit card payment system
  #not being used in first phase.

  def credit_card
    @credit_card = CreditCard.find(params[:id])
    transaction_id, response = Payment.process(@credit_card, current_donor)

    unless transaction_id.present?
      @donation = Donation.build_from_response('credit_card', current_donor, response)
      if @donation.save!
        flash[:notice] = "Donation succesfully accepted"
        redirect_to donations_path(:mode_of_payment => 'credit_card_donations')
      else
        flash[:notice] = "Sorry Donation could not be accepted."
        redirect_to root_url
      end
    else
      flash[:notice] = "Enter the correct credit card information."
      redirect_to donor_path(current_donor.id)
    end
  end


end

