class SearchController < ApplicationController

  before_filter :initialize_search

  def causes
    @causes = @search_helper.find_causes(params[:search_string], :page => params[:page], :per_page => 5)
    @search_string = params[:search_string]
    if session[:current_user] == 'admin'
      render :search_results, :layout => 'admin/default'
    else
      render :search_results
    end
  end

  def search_results
#    render :layout => 'admin'
  end

  def initialize_search
    @search_helper = Object.new.extend(SearchHelper)
  end
end
