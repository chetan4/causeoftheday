class DonationsController < ApplicationController

  before_filter :find_donation, :only => [:show]

  def index
    @donations = eval("current_donor.#{params[:mode_of_payment]}.paginate(:page => params[:page], :per_page => 10)")
    influencer = Donor.find_by_uuid(session[:uuid])
    if !session[:uuid].blank? && influencer != current_donor
      DonorConnection.create!(:donor => current_donor, :cause => Donation.last.cause, :influencer => influencer)
    end
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @donations }
    end
  end
  
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @donation }
    end
  end

  def my_donations
    @donations = current_donor.donations
  end

  private

  def find_donation
    @donation = Donation.find(params[:id])
  end
  
end
