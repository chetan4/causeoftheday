class CausesController < ApplicationController

  before_filter :should_be_authority, :except => :show

  #before_filter :find_organisation, :except => :tagged_causes
  before_filter :find_causes, :only => [:index]
  before_filter :set_organisation, :except => :show



  layout :select_layout

  def index
    if params[:status]
      status = params[:status].titleize
    else
      status = "All"
    end
    @title = "Listing causes - #{status}"
  end

  def show
    @cause = Cause.find(params[:id])
    @cause_updates = @cause.updates
    @cause_images = @cause.images
    render :layout => 'application'
  end

  def new
    @cause = Cause.new
  end

  def create
    @cause = Cause.new(params[:cause])
    @cause.organisation = @organisation if @organisation
    @cause.set_tag_list_on(:cause, params[:cause][:tag_list])
    if @cause.save
      flash[:notice] = "Successfully created cause."
      redirect_to cause_namespaced(causes_path)
    else
      render ('/causes/new')
    end
  end

  def edit
    @cause = Cause.find(params[:id])
  end

  def update
    @cause = Cause.find(params[:id])
    @cause.set_tag_list_on(:cause, params[:cause][:tag_list])
    logger.info(params[:cause][:start_date])
    logger.info(params[:cause][:end_date])
    if @cause.update_attributes(params[:cause])
      flash[:notice] = "Successfully updated cause."
      redirect_to cause_namespaced(causes_path)
    else
      render :action => 'edit'
    end
  end

  def destroy
    @cause = Cause.find(params[:id])
    @cause.destroy
    flash[:notice] = "Successfully destroyed cause."
    redirect_to cause_namespaced(causes_path)
  end

  def tagged_causes
    @causes = Cause.tagged_with(params[:tag]).paginate(:page => params[:page], :per_page => 5)
    @tag = params[:tag]
  end

  def find_organisation
    @organisation = Organisation.find(params[:organisation_id])
  end

  def stats
    @cause = Cause.find(params[:id])
    @countries = @cause.geo_graph_data
  end

  private

  def find_causes
    status = params[:status]
    # executed is not database status, but time constrained status. 
    # so need to take care of it differently.
    status = 'scheduled' if params[:status]=='executed'
    if params[:status]=='executed'
      @causes = Cause.executed.filter_by_status(status).filter_by_organisation(current_organisation_user).paginate(:per_page => 10, :page => params[:page] || 1)
    else
      @causes = Cause.filter_by_status(status).filter_by_organisation(current_organisation_user).paginate(:per_page => 10, :page => params[:page] || 1)
    end
  end

  def set_organisation
    @organisation = Organisation.find(current_organisation_user.organisation_id) unless admin_user?
  end

end
