class Admin::OrganisationsController < Admin::BaseController
  
  def index
    @organisations = Organisation.paginate(:page => params[:page], :per_page => 10).includes(:causes, :organisation_users)
  end
  
  def show
    @organisation = Organisation.find(params[:id])
    @causes = @organisation.causes.paginate(:page => params[:page], :per_page => 10) if @organisation
  end

  def new
    @organisation = Organisation.new
    @organisation.organisation_users.build
  end
  
  def create
    @organisation = Organisation.new(params[:organisation])
    if @organisation.save
      flash[:notice] = "Successfully created organisation."
      redirect_to admin_organisation_path(@organisation)
    else
      render :action => 'new'
    end
  end
  
  def edit
    @organisation = Organisation.find(params[:id])
  end
  
  def update
    #if password is blank, don't update it.
    if params[:organisation]['organisation_users_attributes']['0']['password'].blank?
      params[:organisation]['organisation_users_attributes']['0'].delete('password') 
      params[:organisation]['organisation_users_attributes']['0'].delete('password_confirmation') 
    end
    logger.info params[:organisation].inspect
    logger.info params[:organisation][:organisation_users_attributes]['0'][:password].blank?
    @organisation = Organisation.find(params[:id])
    if @organisation.update_attributes(params[:organisation])
      flash[:notice] = "Successfully updated organisation."
      redirect_to admin_organisation_path(@organisation)
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    @organisation = Organisation.find(params[:id])
    @organisation.destroy
    flash[:notice] = "Successfully destroyed organisation."
    redirect_to admin_organisations_path
  end
end
