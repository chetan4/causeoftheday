class Admin::SchedulesController < Admin::BaseController
  layout 'admin/plain'

  def fetch
    if params[:method] == 'list'
      display_all
    elsif params[:method] == 'update'
     update_schedule
    elsif params[:method] == 'add'
      render :text => 'test'
    end
  end

  def new
    render :template => '/admin/schedules/new', :layout => false
  end

  private
  def display_all
    @causes = []
    Cause.scheduled.each do |cause|
      @causes << cause.schedule_data
    end
    render :json => {:events => @causes}.to_json
  end

  def update_schedule
    cause = Cause.find(params[:calendarId])
    if cause.update_attributes(:schedule_date => Date.parse(params[:CalendarStartTime].split(" ")[0]))
      render :json => {'IsSuccess' => true, 'Msg' => "Updated schedule date successfully"}
    else
      render :json => {:error => "Some other cause is already scheduled for the selected date"}
    end
  end

end
