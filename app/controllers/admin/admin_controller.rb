class Admin::AdminController < Admin::BaseController

  def index
    redirect_to admin_dashboard_path
  end

  def logout
    session[:current_user] = nil
    reset_session
    respond_to do |format|
      format.html { redirect_to(root_url) }
      format.xml  { head :ok }
    end
  end
end

