class Admin::DashboardController < Admin::BaseController

  def index
    @total_organisations = Organisation.count
    @causes_count = Cause.count
    @causes_awaiting_review = Cause.in_review.count
    @causes_in_making = Cause.in_making.count
    @causes_scheduled = Cause.scheduled.count
    @causes_executed = Cause.executed.count
  end
  
end
