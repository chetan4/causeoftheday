class AuthorizationsController < ApplicationController

  def welcome
  end

  def create
    omniauth = request.env["omniauth.auth"]
    authentication = Authorization.find_by_provider_and_uid(omniauth['provider'], omniauth['uid'])
    if authentication
      flash[:notice] = "Welcome back #{omniauth['provider']} user."
      update_twitter_facebook_tokens(authentication.donor, omniauth)
      sign_in_and_redirect(:donor, authentication.donor)
    elsif current_donor
      current_donor.create_authorization(omniauth)
      flash[:notice] = "Successfully added #{omniauth['provider']} authentication."
      update_twitter_facebook_tokensns(current_donor, omniauth)
      redirect_to authentications_url
    else
      donor = Donor.new
      logger.info omniauth.inspect
      donor.apply_omniauth(omniauth)
      update_twitter_facebook_tokens(donor, omniauth)
      if donor.save!(:validate => false)
        flash[:notice] = "Welcome #{omniauth['provider']} user. Your account has been created."
        sign_in_and_redirect(:donor, donor)
      end
    end

  end
  
  def failure
    flash[:notice] = "Sorry, You din't authorize"
    redirect_to root_url
  end
  
  def destroy
    @authorization = current_donor.authorizations.find(params[:id])
    flash[:notice] = "Successfully deleted #{@authorization.provider} authentication."
    @authorization.destroy
    redirect_to root_url
  end

  def facebook_provider?
    omniauth['provider']=='facebook'
  end

  private

  def update_twitter_facebook_tokens(donor, omniauth)
      donor.populate_facebook_information(omniauth)
      donor.populate_twitter_information
  end
end
