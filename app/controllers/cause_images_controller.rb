class CauseImagesController < Admin::BaseController

  skip_before_filter :authenticate_admin
  
  def new
    @cause = Cause.find(params[:cause_id])
  end

  def create
    @cause = Cause.find(params[:cause_id])
    #@cause.images << CauseImage.new(params[:cause_image])
    @cause_image = @cause.images.build(params[:cause_image])
    if @cause_image.save
      redirect_to cause_namespaced( new_cause_cause_image_path(@cause))
    else
      render :action => :new
    end
  end

  def show

  end

  def destroy
    @cause = Cause.find(params[:cause_id])
    CauseImage.find(params[:id]).destroy
    redirect_to cause_namespaced(new_cause_cause_image_path(@cause))
  end
end
