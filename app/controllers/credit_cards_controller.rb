class CreditCardsController < ApplicationController
  before_filter :authenticate_donor!, :except => [:destroy]
  def index
    @credit_cards = current_donor.credit_cards
  end
  
  def show
    @credit_card = CreditCard.find(params[:id])
  end
  
  def new
    @credit_card = CreditCard.new
  end
  
  def create
    @credit_card = CreditCard.new(params[:credit_card])
    @credit_card.donor_id = current_donor.id
    @credit_card.ip_address = request.remote_ip
    if @credit_card.save
      redirect_to new_address_path(:type_id => @credit_card.id , :type => @credit_card.class), :layout => 'application'
    else
      render :action => 'new'
    end
  end

  def edit
    @credit_card = CreditCard.find(params[:id])
  end
  
  def update
    @credit_card = CreditCard.find(params[:id])
    @credit_card.expiration_month = params[:date][:month]
    if @credit_card.update_attributes(params[:credit_card])
      flash[:notice] = "Successfully updated credit card."
      redirect_to donor_path(current_donor.id)
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    @credit_card = CreditCard.find(params[:id])
    @credit_card.destroy
    flash[:notice] = "Successfully destroyed credit card."
    redirect_to donor_path(current_donor.id)
  end

  
  
end
