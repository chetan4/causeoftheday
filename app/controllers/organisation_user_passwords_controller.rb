class OrganisationUserPasswordsController < Devise::PasswordsController
  layout 'organisation'

  def create
    unless params[:organisation_user][:email].present?
      flash[:error] = 'Please type your email id.'
      render :action => 'new'
    else
      if OrganisationUser.find_by_email(params[:organisation_user][:email]).present?
        super
      else
        flash[:error] = 'Invalid email id.'
        render :action => 'new'
      end
    end
  end
end
