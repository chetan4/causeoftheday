class SigninResponseController < ApplicationController
  def facebook
    donor = FacebookAccount.signin(params)
    current_donor.sign_out if current_donor
    sign_in donor
    render :nothing => true
  end
end
