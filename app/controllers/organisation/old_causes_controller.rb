class Organisation::OldCausesController < Organisation::BaseController

  before_filter :check_status,:only => [:edit, :update, :remove]

  def index
    @causes = Organisation.find(current_organisation_user.organisation).causes.not_cancelled.paginate(:per_page => 10, :page => params[:page] || 1)
  end

  def new
    @cause = Cause.new
  end

  def create
    organisation = current_organisation_user.organisation
    @cause = organisation.causes.new(params[:cause])
    if @cause.save
      flash[:notice] = "Successfully created cause."
      redirect_to organisation_causes_path
    else
      render :action => 'new'
    end
  end

  def edit
    @cause = Cause.find(params[:id])
  end

  def action_not_permitted
    @cause = Cause.find(params[:cause_id])
  end

  def update
    @cause = Cause.find(params[:id])
    if @cause.update_attributes(params[:cause])
      flash[:notice] = "Successfully updated cause."
      redirect_to organisation_causes_path
    else
      render :action => 'edit'
    end
  end

  def remove
    Cause.find(params[:cause_id]).cancel
    redirect_to organisation_causes_path
  end

  def request_approval
    Cause.find(params[:cause_id]).mark_for_review
    redirect_to organisation_causes_path
  end

  private
  def check_status
    @cause = Cause.find(params[:id])
    if !@cause.in_making?
      redirect_to cause_action_not_permitted_path(@cause)
    end
  end

end
