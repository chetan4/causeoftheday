class Organisation::DashboardController < Organisation::BaseController

  def index
    @organisation = current_organisation_user.organisation
    @causes = @organisation.causes
  end

  def terms_and_conditions

  end

  def faq

  end
end
