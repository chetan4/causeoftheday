class Organisation::BaseController < ApplicationController
  before_filter :authenticate_organisation_user!
  layout 'organisation'
end
