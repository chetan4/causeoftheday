class Organisation::OrganisationUsersController < Organisation::BaseController

  def show
    @organisation_user = OrganisationUser.find(params[:id])
  end

  def edit
    @organisation_user = OrganisationUser.find(params[:id])
  end

  def update
    @organisation_user = OrganisationUser.find(params[:id])

    unless params[:organisation_user][:password].present?
      params[:organisation_user].delete(:password)
      params[:organisation_user].delete(:password_confirmation)
    end
    logger.info params.inspect
    logger.info '-----------------------------'

    if @organisation_user.update_attributes(params[:organisation_user])
      flash[:notice] = "Successfully updated your profile."
      render :action => 'show'
    else
      render :action => 'edit'
    end
  end
end
