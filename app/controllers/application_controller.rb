require 'yaml'

class ApplicationController < ActionController::Base

  #helper :all
  protect_from_forgery
  before_filter :generate_tag_cloud, :cause_of_the_day

  def cause_of_the_day
    @cause_of_the_day = Cause.find :first, :conditions => {:schedule_date => Date.today}
  end

  def generate_tag_cloud
    @tags = Cause.tag_counts_on(:cause)
  end

  helper_method :admin_user?, :cause_namespaced

  def admin_user?
    session[:current_user]=='admin'
  end

  def cause_namespaced(path)
    user_namespace + path
  end

  def user_namespace
    admin_user? ? '/admin' : '/organisation'
  end


  def select_layout
    if admin_user?
      'admin/default'
    elsif current_organisation_user
      'organisation'
    else
      'application'
    end
  end

  def should_be_authority
    unless authority?
      redirect_to :root, :notice => 'You are not allowed to access this page.'
    end
  end

  protected

  def authority?
    admin_user? || organisation_user_signed_in?
  end

  def authenticate_admin
    # The piece of code below is using metal controller to tell Warden
    # to return a custom 401 response from your controller

    #env['warden'].custom_failure!
    authenticate_or_request_with_http_basic do |username, password|
      session[:current_user] = 'admin' if username == SiteConfig.admin.username && password == SiteConfig.admin.password
    end
  end

end
