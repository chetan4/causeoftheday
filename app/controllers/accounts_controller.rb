class AccountsController < ApplicationController

  def welcome
  end

  def create
    omniauth = request.env["omniauth.auth"]
    account = Account.authorize(omniauth, current_donor)
    if account
      flash[:notice] = 'Welcome to 97causes.'
      sign_in_and_redirect(:donor, account.donor)
    else
      flash[:error] = 'Error signing you in !!!'
      redirect_to root_path
    end
  end
  
  def failure
    flash[:notice] = "Sorry, You din't authorize"
    redirect_to root_url
  end
  
  def destroy
    @authorization = current_donor.authorizations.find(params[:id])
    flash[:notice] = "Successfully deleted #{@authorization.provider} authentication."
    @authorization.destroy
    redirect_to root_url
  end

end
