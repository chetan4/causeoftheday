class AddressesController < ApplicationController
  def index
    @addresses = Address.all
  end
  
  def show
    @address = Address.find(params[:id])
  end
  
  def new
    @address = Address.new(:owner_id => params[:type_id], :owner_type => params[:type])
    if session[:current_user] == 'admin'
     render :layout => 'admin'
    end
  end
  
  def create
    @address = Address.new(params[:address])
    if @address.save!
      flash[:notice] = "Successfully created address."
      if session[:current_user] == 'admin'
        redirect_to admin_organisation_path(:id => params[:address][:owner_id])
      elsif current_donor
        redirect_to donor_path(:id => current_donor.id, :owner_id => params[:type_id], :owner_type => params[:type])
      end
    else
      render :action => 'new'
    end
  end
  
  def edit
    @address = Address.find_by_id_and_owner_id_and_owner_type(params[:id], params[:owner_id], params[:owner_type])
    if session[:current_user] == 'admin'
     render :layout => 'admin'
    end
  end
  
  def update
    @address = Address.find_by_id_and_owner_id_and_owner_type(params[:id], params[:address][:owner_id], params[:address][:owner_type])
    if @address.update_attributes(params[:address])
      flash[:notice] = "Successfully updated address."
      if session[:current_user] == 'admin'
        redirect_to admin_organisation_path(:id => params[:address][:owner_id])
      elsif current_donor
        redirect_to donor_path(current_donor.id)
      end
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    @address = Address.find(params[:id])
    @address.destroy
    flash[:notice] = "Successfully destroyed address."
    if session[:current_user] == 'admin'
        redirect_to admin_organisation_path(:id => params[:owner_id])
      elsif current_donor
        redirect_to donor_path(current_donor.id)
    end
    
  end
end
