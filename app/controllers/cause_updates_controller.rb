class CauseUpdatesController < ApplicationController

  before_filter :set_cause

  layout :select_layout
  # GET /cause_updates
  # GET /cause_updates.json
  def index
    @cause_updates = @cause.updates.paginate :per_page => 10, :page => params[:page]

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @cause_updates }
    end
  end

  # GET /cause_updates/1
  # GET /cause_updates/1.json
  def show
    @cause_update = @cause.updates.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @cause_update }
    end
  end

  # GET /cause_updates/new
  # GET /cause_updates/new.json
  def new
    @cause_update = @cause.updates.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @cause_update }
    end
  end

  # GET /cause_updates/1/edit
  def edit
    @cause_update = CauseUpdate.find(params[:id])
  end

  # POST /cause_updates
  # POST /cause_updates.json
  def create
    @cause_update = @cause.updates.new(params[:cause_update])

    respond_to do |format|
      if @cause_update.save
        format.html { redirect_to cause_namespaced(cause_cause_update_path(@cause, @cause_update)), :notice => 'Cause update was successfully created.' }
        format.json { render :json => @cause_update, :status => :created, :location => @cause_update }
      else
        format.html { render :action => "new" }
        format.json { render :json => @cause_update.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /cause_updates/1
  # PUT /cause_updates/1.json
  def update
    @cause_update = @cause.updates.find(params[:id])

    respond_to do |format|
      if @cause_update.update_attributes(params[:cause_update])
        format.html { redirect_to cause_namespaced(cause_cause_update_path(@cause, @cause_update)), :notice => 'Cause update was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @cause_update.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /cause_updates/1
  # DELETE /cause_updates/1.json
  def destroy
    @cause_update = @cause.updates.find(params[:id])
    @cause_update.destroy

    respond_to do |format|
      format.html { redirect_to cause_namespaced(cause_cause_updates_path(@cause)) }
      format.json { head :ok }
    end
  end

  private
  def set_cause
    @cause = Cause.find(params[:cause_id])
  end
end
