//= require jquery
//= require jquery_ujs
//= require tinymce-jquery
//= require_self
//= require 'common/jquery-ui-1.8.16.custom.min'
//= require 'common/jquery.pngFix.js'
//= require 'donor/global'


$(function() {
    $('.applyTinymce').tinymce({
        theme: 'advanced',
        height: '400px'
    });
});
