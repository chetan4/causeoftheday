$(document).ready(function(){
    $('img,section,a').pngFix();
});


function detectTimeZone(){
    var dtDate = new Date('1/1/' + (new Date()).getUTCFullYear());
    var intOffset = 10000; //set initial offset high so it is adjusted on the first attempt
    var intMonth;
    var intHoursUtc;
    var intHours;
    var intDaysMultiplyBy;

    //go through each month to find the lowest offset to account for DST
    for (intMonth=0;intMonth < 12;intMonth++){
        //go to the next month
        dtDate.setUTCMonth(dtDate.getUTCMonth() + 1);

        //To ignore daylight saving time look for the lowest offset.
        //Since, during DST, the clock moves forward, it'll be a bigger number.
        if (intOffset > (dtDate.getTimezoneOffset() * (-1))){
            intOffset = (dtDate.getTimezoneOffset() * (-1));
        }
    }

    return intOffset;
}

function loadFacebook(){
    FB.init({
        appId:'165205913533038',
        cookie:true,
        status:true,
        xfbml:true
    });
    FB.login(function(response) {
        if(response.session){
            console.log(response.session);
            url = '/facebook_signin?uid='+response.session.uid + '&access_token=' + response.session.access_token + '&secret='+response.session.secret;
            $.get(url, function(){
                window.location.href = "/home/index";
            });

        }else{
            alert("OOPS But why?");
        }
    }, {perms:'publish_stream,offline_access'});
}