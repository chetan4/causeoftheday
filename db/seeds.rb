def announce(message)
  length = [0, 75 - message.length].max
  puts "== %s %s" % [message, "=" * length]
end

puts ''
announce ''
announce 'PLANTING SEED DATA'
announce ''
puts ''

causes_organisation = Organisation.create!(
    :name               => "97 Changes",
    :organisation_type  => "NGO",
    :website            => 'http://www.97changes.com',
    :organisation_users => [
        OrganisationUser.new(
            :password              => 'passme',
            :password_confirmation => 'passme',
            :username              => "97changes",
            :email                 => "97changes@gmail.com")
    ]
)
announce 'created 97Changes organization'

causes_maintenance_cause = causes_organisation.causes.create(
    :name          => '97Changes Maintenance',
    :target_amount => '',
    :description   => 'Please donate for maintenance of 97changes website. You donations will help us improve this website.',
    :schedule_date => '',
    :country       => 'India',
    :status        => 'permanent',
    :target_amount  => 0,
    :permanent     => true
)
announce 'created maintenance cause for 97changes website'

credit_card_donor = Donor.create!(
    :email                 => 'donor@test.com',
    :username              => 'testdonor',
    :firstname             => 'Test',
    :lastname              => 'Donor',
    :password              => 'passme',
    :password_confirmation => 'passme'
)
announce 'created donor'

master_credit_card = CreditCard.create!(
    :expiration_month => 5.months.since.month,
    :expiration_year  => 5.years.since,
    :number           => '4444-5555-9999-8888',
    :security_code    => '1234',
    :card_type        => 'MASTER',
    :donor            => credit_card_donor
)
announce 'created master credit card'

us_home_address = credit_card_donor.addresses.create(
    :address_line1 => '13625 S48th ST',
    :address_line2 => 'Sonoran Apartments Apt#1210',
    :landline_no   => '480-961-2206',
    :mobile_no     => '605-535-9140',
    :city          => 'Phoenix',
    :state         => 'Arizona',
    :country       => 'USA',
    :zip_code      => '85044',
    :address_type  => 'home'
)
announce 'created home address'

help_india_organisation = Organisation.create!(
    :name               => "Helpage India",
    :organisation_type  => "NGO",
    :website            => 'http://www.HelpageIndia.com',
    :organisation_users => [
        OrganisationUser.new(
            :password              => 'passme',
            :password_confirmation => 'passme',
            :username              => "Helpage_India",
            :email                 => "help_india@ngo.com")
    ]
)
announce 'created helpage india organization'

# organisation can't have password n cofirmation.

girl_child_cause = help_india_organisation.causes.create(
    :name          => 'Save Girl Child',
    :target_amount => '5000',
    :description   => 'Please donate for Save Girl Child cause',
    :schedule_date => 4.day.since,
    :country       => 'Zimbabwe'
)
announce 'created girl cause for helpage india'


donation_by_credit_card = Donation.new(
    :amount     => "100",
    :ip         => "9.9.99.122"
)

credit_card_responses  = CreditCardResponse.new(
    :transaction_id => '12334345'
)

donation_by_credit_card.mode_of_payment          = master_credit_card
donation_by_credit_card.mode_of_payment_response = credit_card_responses
donation_by_credit_card.save!
announce 'created donation for 100 USD'

another_credit_card_donor = Donor.create!(
    :email                 => 'anotherdonor@test.com',
    :username              => 'anothertestdonor',
    :firstname             => 'Test',
    :lastname              => 'Donor',
    :password              => 'passme',
    :password_confirmation => 'passme'
)
announce 'created anothertestdonor'

another_master_credit_card = CreditCard.create!(
    :expiration_month => 5.months.since,
    :expiration_year  => 5.years.since,
    :number           => '4444-5555-9999-77777',
    :security_code    => '4444',
    :card_type        => 'MASTER',
    :donor            => another_credit_card_donor
)
announce 'created test donor master credit card'

us_office_address =  credit_card_donor.addresses.create!(
    :address_line1 => '13625 S48th ST',
    :address_line2 => 'San Melia Apartments Apt#1235',
    :landline_no   => '480-961-2206',
    :mobile_no     => '605-535-9140',
    :city          => 'Phoenix',
    :state         => 'Arizona',
    :country       => 'USA',
    :zip_code      => '85044',
    :owner         => another_master_credit_card,
    :address_type  => 'home'
)
announce 'created test donor address'

cry_organisation = Organisation.create!(
    :name               => "Cry",
    :organisation_type  => "NPO",
    :website            => 'http://www.cry.com',
    :organisation_users => [
        OrganisationUser.new(
            :password              => 'passme',
            :password_confirmation => 'passme',
            :email                 => "Cry@ngo.com",
            :username              => "Cry"
        )
    ]
)
announce 'created cry organization'

each_one_teach_cause = cry_organisation.causes.create!(
    :name          => 'Each One Teach',
    :target_amount => '10000',
    :description   => 'Each One Teach One',
    :schedule_date => 1.day.since,
    :country       => 'Mozambique'
)
announce 'created a cause for cry'


donation_by_another_credit_card = Donation.new(
    :amount     => "100",
    :ip         => "61.12.126.211"
)

another_credit_card_responses = CreditCardResponse.create!(
    :transaction_id => '12334345'
)
donation_by_another_credit_card.mode_of_payment_response = another_credit_card_responses
donation_by_another_credit_card.mode_of_payment          = another_master_credit_card
donation_by_another_credit_card.save!
announce 'created donation by another donor'


amazon_donor = Donor.create!(
    :email                 => 'amazon_donor@test.com',
    :username              => 'amazon_testdonor',
    :firstname             => 'Amazon',
    :lastname              => 'Test',
    :password              => 'passme',
    :password_confirmation => 'passme'
)
announce 'created amazon donor'

visa_credit_card = CreditCard.create!(
    :expiration_month => 5.months.since,
    :expiration_year  => 5.years.since,
    :number           => '4444-5555-9999-2222',
    :security_code    => '1111',
    :card_type        => 'VISA',
    :donor            => amazon_donor
)
announce 'created visa credit card'

india_home_address = credit_card_donor.addresses.create!(
    :address_line1 => 'Ginni Bldg, Gurunanak Rd',
    :address_line2 => 'Dehuroad',
    :landline_no   => '480-961-2206',
    :mobile_no     => '605-535-9140',
    :city          => 'Pune',
    :state         => 'Maharashtra',
    :country       => 'India',
    :zip_code      => '421201',
    :owner         => visa_credit_card,
    :address_type  => 'home'
)
announce 'created india home address'

save_tiger_organisation = Organisation.create!(
    :name               => "Save Tiger",
    :organisation_type  => "NGO",
    :website            => 'http://www.SaveTiger.com',
    :organisation_users => [
        OrganisationUser.new(
            :username              => "Save Tiger",
            :email                 => "save_tiger@private.com",
            :password              => 'passme',
            :password_confirmation => 'passme'
        )
    ]
)
announce 'created save tiger organization'

save_tiger_cause = save_tiger_organisation.causes.create!(
    :name          => 'Save Tiger',
    :target_amount => '10000',
    :description   => 'Save All Tigers',
    :schedule_date => Date.today,
    :country       => 'South Africa'
)
announce 'created save tiger cause'


donation_by_amazon  = Donation.new(
    :amount          => "100",
    :mode_of_payment => visa_credit_card,
    :ip              => "9.9.99.133"
)
amazon_responses = AmazonResponse.new(
    :transaction_id => '12334345'
)

donation_by_amazon.mode_of_payment_response = amazon_responses
donation_by_amazon.mode_of_payment          = visa_credit_card
donation_by_amazon.save!
announce 'created donation by amazon donor'

another_amazon_donor = Donor.create!(
    :email                 => 'anotheramazondonor@test.com',
    :username              => 'anotheramazontestdonor',
    :firstname             => 'Test',
    :lastname              => 'anotheramazonDonor',
    :password              => 'passme',
    :password_confirmation => 'passme'
)
announce 'created anotheramazontestdonor'

another_visa_credit_card = CreditCard.create!(
    :expiration_month => 5.months.since,
    :expiration_year  => 5.years.since,
    :number           => '4444-5555-9999-4444',
    :security_code    => '2222',
    :card_type        => 'VISA',
    :donor            => another_amazon_donor
)

announce 'created visa credit card for anotheramazontestdonor'

india_office_address = another_amazon_donor.addresses.create!(
    :address_line1 => '106 East, Main Bazar',
    :address_line2 => 'Ratan Apart. Flat # 501',
    :landline_no   => '111-222-2206',
    :mobile_no     => '605-535-9140',
    :city          => 'Pune',
    :state         => 'Maharashtra',
    :country       => 'India',
    :zip_code      => '412115',
    :address_type  => 'office',
    :owner         => amazon_donor
)
announce 'created address for anotheramazontestdonor'

teach_india_organisation = Organisation.create!(
    :name               => "Teach India",
    :organisation_type  => "NGO",
    :website            => 'http://www.facebook.com/',
    :organisation_users => [
        OrganisationUser.new(
            :username              => "Teach India",
            :email                 => "teach_india@npo.com",
            :password              => 'passme',
            :password_confirmation => 'passme'
        )
    ]
)
announce 'created Teach India organization'

aids_awareness_cause = teach_india_organisation.causes.create!(
    :name          => 'AIDS Awareness',
    :target_amount => '15000',
    :description   => 'Lets create awareness for AIDS',
    :schedule_date => 2.day.since,
    :country       => 'Kazakhstan'
)
announce 'created cause for aids awareness for Teach India organization'


another_donation_by_amazon  = Donation.new(
    :amount     => "100",
    :ip         => "9.9.99.44"
)

another_donation_by_amazon.mode_of_payment          = another_visa_credit_card
another_donation_by_amazon.mode_of_payment_response = AmazonResponse.new
another_amazon_responses = AmazonResponse.create!(
    :transaction_id => '12334345'
)
another_donation_by_amazon.save!
announce 'created donation by amazon'
puts ''