class AddTandcToCause < ActiveRecord::Migration
  def self.up
    add_column :causes, :accepted_terms, :boolean
  end

  def self.down
    remove_column :causes, :accepted_terms
  end
end
