class CreateDonorConnections < ActiveRecord::Migration
  def self.up
    create_table :donor_connections do |t|
      t.references :donor
      t.integer :influencer_id
      t.integer :cause_id
      t.timestamps
    end
  end

  def self.down
    drop_table :donor_connections
    remove_column :donors, :uuid
  end
end
