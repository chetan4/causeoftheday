class CreatePaypalResponses < ActiveRecord::Migration
  def self.up
    create_table :paypal_responses do |t|
      t.string :payment_status
      t.float :tax
      t.string :receiver_email
      t.string :receiver_id
      t.integer :quantity
      t.string :business
      t.float :payment_gross
      t.string :transaction_subject
      t.string :pending_reason
      t.float :notify_version
      t.float :payment_fee
      t.string :mc_currency
      t.string :txn_id
      t.string :verify_sign
      t.string :item_name
      t.string :test_ipn
      t.string :txn_type
      t.string :charset
      t.float :mc_gross
      t.float :mc_fee
      t.string :payer_id
      t.string :last_name
      t.string :custom
      t.string :payer_status
      t.string :payer_email
      t.string :protection_eligibility
      t.string :residence_country
      t.datetime :payment_date
      t.string :item_number
      t.string :first_name
      t.string :payment_type

      t.timestamps
    end
  end

  def self.down
    drop_table :paypal_responses
  end
end
