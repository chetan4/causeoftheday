class BaseSchema < ActiveRecord::Migration
  def self.up

    create_table "addresses", :force => true do |t|
      t.string   "address_line1"
      t.string   "address_line2"
      t.integer  "landline_no"
      t.integer  "mobile_no"
      t.string   "city"
      t.string   "state"
      t.string   "country"
      t.integer  "zip_code"
      t.integer  "owner_id"
      t.string   "owner_type"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "address_type"
    end

    create_table "accounts", :force => true do |t|
      t.string   "provider"
      t.string   "uid"
      t.integer  "donor_id"
      t.string   "type"
      t.string   "profile_image_url"
      t.string   "gender"
      t.string   "access_token"
      t.string   "secret"
      t.string   "connections_count"
      t.text     "user_info"  #will have marshal dump of user_info hash.
      t.timestamps
    end

    create_table "amazons", :force => true do |t|
      t.integer  "donor_id"
      t.string   "email"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "amazon_responses", :force => true do |t|
      t.string  "payment_reason"
      t.string  "buyer_name"
      t.string  "signature_version"
      t.string  "transaction_date"
      t.string  "certificate_url"
      t.string  "recipient_name"
      t.string  "signature"
      t.string  "recipient_email"
      t.string  "signature_method"
      t.string  "transaction_id"
      t.string  "reference_id"
      t.string  "payment_method"
      t.string  "operation"
      t.string  "status"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "causes", :force => true do |t|
      t.string   "name"
      t.integer  "target_amount"
      t.text   "description"
      t.integer  "organisation_id"
      t.date     "schedule_date"
      t.string   "city"
      t.string   "country"
      t.boolean  "permanent", :default => false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "credit_cards", :force => true do |t|
      t.integer  "donor_id"
      t.string   "ip_address"
      t.string  "encrypted_number"
      t.string  "encrypted_security_code"
      t.string   "card_type"
      t.string   "expiration_month"
      t.integer  "expiration_year"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "credit_card_responses", :force => true do |t|
      t.integer  "transaction_id"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "donations", :force => true do |t|
      t.integer  "cause_id"
      t.integer  "donor_id"
      t.integer  "mode_of_payment_id"
      t.integer  "mode_of_payment_response_id"
      t.float  "amount"
      t.string   "ip"
      t.string   "mode_of_payment_type"
      t.string   "mode_of_payment_response_type"
      t.string   "country"
      t.string   "city"
      t.boolean  "broadcasted"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "donors", :force => true do |t|
      t.string   "username"
      t.string   "firstname"
      t.string   "middlename"
      t.string   "lastname"
      t.string   "uuid"
      t.database_authenticatable :null => true
                                 #      t.confirmable
      t.recoverable
      t.rememberable
      t.trackable
      t.date "bdate"
      t.timestamps
    end

    create_table "organisations", :force => true do |t|
      t.string   "name"
      t.string  "organisation_type"
      t.string   "website"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    #    add_index :donors, :email,                :unique => true
    #    add_index :donors, :confirmation_token,   :unique => true
    add_index :donors, :reset_password_token, :unique => true

  end

  def self.down
    drop_table 'addresses'
    drop_table 'amazons'
    drop_table 'amazon_responses'
    drop_table 'credit_card_responses'
    drop_table 'authorizations'
    drop_table 'causes'
    drop_table 'credit_cards'
    drop_table 'donations'
    drop_table 'donors'
    drop_table 'organisations'
  end
end

