class AddStartDateAndEndDateToCause < ActiveRecord::Migration
  def change
    add_column :causes, :start_date, :date
    add_column :causes, :end_date, :date
    add_column :causes, :requested_start_date, :date
    add_column :causes, :requested_end_date, :date
  end
end
