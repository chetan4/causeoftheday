class AddStartEndTimeToCause < ActiveRecord::Migration
  def self.up
    add_column :causes, :start_time, :datetime
    add_column :causes, :end_time, :datetime
  end

  def self.down
    remove_column :causes, :start_time
    remove_column :causes, :end_time
  end
end
