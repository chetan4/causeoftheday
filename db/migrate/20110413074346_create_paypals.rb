class CreatePaypals < ActiveRecord::Migration
  def self.up
    create_table :paypals do |t|
      t.integer  "donor_id"
      t.string   "email"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.timestamps
    end
  end

  def self.down
    drop_table :paypals
  end
end
