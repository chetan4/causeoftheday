class RemoveAvtarFromCause < ActiveRecord::Migration
  def self.up
    remove_column :causes, :avatar_file_name
    remove_column :causes, :avatar_content_type
    remove_column :causes, :avatar_file_size
    remove_column :causes, :avatar_updated_at
  end

  def self.down
    add_column :causes, :avatar_file_name, :string
    add_column :causes, :avatar_content_type, :string
    add_column :causes, :avatar_file_size, :integer
    add_column :causes, :avatar_updated_at, :datetime
  end
end
