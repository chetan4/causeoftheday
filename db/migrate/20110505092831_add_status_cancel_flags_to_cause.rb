class AddStatusCancelFlagsToCause < ActiveRecord::Migration
  def self.up
    add_column :causes, :status, :string, :default => 'in_making'
    add_column :causes, :cancelled, :boolean, :default => false
  end

  def self.down
    remove_column :causes, :status
    remove_column :causes, :cancelled
  end
end
