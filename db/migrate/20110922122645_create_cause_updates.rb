class CreateCauseUpdates < ActiveRecord::Migration
  def change
    create_table :cause_updates do |t|
      t.string :title
      t.text :description
      t.integer :cause_id

      t.timestamps
    end
  end
end
