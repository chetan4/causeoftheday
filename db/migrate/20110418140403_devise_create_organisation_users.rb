class DeviseCreateOrganisationUsers < ActiveRecord::Migration
  def self.up
    create_table(:organisation_users) do |t|
      t.references :organisation
      t.string :first_name
      t.string :last_name
      t.database_authenticatable :null => false
      t.recoverable
      t.rememberable
      t.trackable
      t.string :persistence_token
      # t.confirmable
      # t.lockable :lock_strategy => :failed_attempts, :unlock_strategy => :both
      # t.token_authenticatable
      t.timestamps
    end

    add_index :organisation_users, :email,                :unique => true
    add_index :organisation_users, :reset_password_token, :unique => true
    # add_index :organisation_users, :confirmation_token,   :unique => true
    # add_index :organisation_users, :unlock_token,         :unique => true
  end

  def self.down
    drop_table :organisation_users
  end
end
