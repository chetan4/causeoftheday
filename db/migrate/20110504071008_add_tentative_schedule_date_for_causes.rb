class AddTentativeScheduleDateForCauses < ActiveRecord::Migration
  def self.up
    add_column :causes, :requested_schedule_date, :date
  end

  def self.down
    remove_column :causes, :requested_schedule_date
  end
end
